# Core Data Import (cdi)

Der Stammdatenimport stellt Felder und Entitäten für Stammdaten bereit. Diese Stammdaten werden in jedem drupalbasiertem Projekt der LWB benötigt. Die Stammdaten werden taglich vom Server [wolf.lwb.local](https://pma.core.web1.lwb.local/) importiert. Nicht mehr vorhandete Daten, z.B. ausgetretende Mitarbeiter oder geschlossende Kostenstellen, werden ausschließlich deaktiviert, da ein Löschen Querverweise innerhalb der Daten zerstören könnte. Ein Löschkonzept kann über Module wie [Rules](https://www.drupal.org/project/rules) umgesetzt werden.

Der tägliche Import wird vom [Server](https://git.web1.lwb.local/server/docker) gestartet. Das Ergebnis des Imports wird in ein [zentrales Log](https://grafana.web1.lwb.local) geschrieben. Je nach Projekt werden die zugehörigen Logs überwacht und entsprechend bewarnt.

## Features

* Import von Stammdaten aus der Oberfläche oder durch Drush
* Bereitstellung eines Anlegen-Buttons für Inhalt anhand der URL

## Sub-Module

Abhängigkeiten unter den Modulen wurden hinterlegt. Z.B. wenn des Modul cdi_user aktiviert wird wird automatisch das Modul cdi_costcenters aktiviert, da zu jedem Mitarbeiter auch eine Kostenstelle gehört.

* Kostenstelle [cdi_costcenter](modules/cdi_costcenter)
* Buchungskreise [cdi_companycode](modules/cdi_companycode)
* Instandhaltungsbezirk [cdi_district](modules/cdi_district)
* Funktion [cdi_function](modules/cdi_function)
* Organisationseinheit [cdi_organisationalunit](modules/cdi_organisationalunit)
* Mitarbeiter [cdi_user](modules/cdi_user)
* Hilfsmodul für Berechtigungen [cdi_node_access](modules/cdi_node_access)

## Voraussetzungen

* Eine Datenbank mit den Stammdaten muss in der settings.php konfiguriert werden
* [cocur/slugify](https://packagist.org/packages/cocur/slugify)
* [Config Ignore](https://www.drupal.org/project/config_ignore)
* [Field Group](https://www.drupal.org/project/field_group)
* [Node View Permissions](https://www.drupal.org/project/node_view_permissions)

## Drush

* cdi:core:import: Startet den Stammdatenimport
