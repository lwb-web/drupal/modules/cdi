# Core Data Import - Buchungskreis (cdi_companycode)

## Features

* Import von Buchungskreisen

## Berechtigungen

* add cdi_companycode entities: Anlegen von Buchungskreisen
* administer cdi_companycode entities: Administration von Buchungskreisen
* delete cdi_companycode entities: Löschen von Buchungskreisen
* edit cdi_companycode entities: Editieren von Buchungskreisen
* view published cdi_companycode entities: Anzeigeberechtigung von veröffentlichten Buchungskreisen
* view unpublished cdi_companycode entities: Anzeigeberechtigung von unveröffentlichten Buchungskreisen

## Felder

* ID
* Beschreibung
* Langbeschreibung (ID + Beschreibung)
