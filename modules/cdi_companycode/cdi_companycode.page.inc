<?php

/**
 * @file
 * Contains cdi_companycode.page.inc.
 *
 * Page callback for Core - Company Code entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Core - Company Code templates.
 *
 * Default template: cdi_companycode.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cdi_companycode(array &$variables) {
  // Helpful $content variable for templates.

  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
  $localTasksBlock = \Drupal::service('plugin.manager.block')->createInstance('local_tasks_block', []);
  $variables['local_tasks'] = $localTasksBlock->build();
}
