<?php

namespace Drupal\cdi_companycode;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Core - Company Code entity.
 *
 * @see \Drupal\cdi_companycode\Entity\CdiCompanyCode.
 */
class CdiCompanyCodeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\cdi_companycode\Entity\CdiCompanyCodeInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cdi_companycode entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published cdi_companycode entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit cdi_companycode entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete cdi_companycode entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cdi_companycode entities');
  }

}
