<?php

namespace Drupal\cdi_companycode;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Core - Company Code entities.
 *
 * @ingroup cdi_companycode
 */
class CdiCompanyCodeListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Core - Function ID');
    $header['name'] = $this->t('Name');
    $header['owner'] = $this->t('Author');
    $header['date_created'] = $this->t('Date created');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\cdi_function\Entity\CdiFunction $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.cdi_companycode.canonical',
      ['cdi_companycode' => $entity->id()]
    );
    $row['owner'] = $entity->getOwner()->toLink($entity->getOwner()->label());
    $row['date_created'] = date('d-m-Y H:m', $entity->getCreatedTime());
    return $row + parent::buildRow($entity);
  }

}
