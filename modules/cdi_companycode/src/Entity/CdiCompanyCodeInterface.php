<?php

namespace Drupal\cdi_companycode\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Core - Company Code entities.
 *
 * @ingroup cdi_companycode
 */
interface CdiCompanyCodeInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Core - Company Code title.
   *
   * @return string
   *   Title of the Core - Company Code.
   */
  public function getTitle();

  /**
   * Sets the Core - Company Code title.
   *
   * @param string $value
   *   The Core - Company Code title.
   *
   * @return \Drupal\cdi_companycode\Entity\CdiCompanyCodeInterface
   *   The called Core - Company Code entity.
   */
  public function setTitle($value);

  /**
   * Gets the Core - Company Code Identifier.
   *
   * @return string
   *   Identifier of the Core - Company Code.
   */
  public function getIdentifier();

  /**
   * Sets the Core - Company Code Identifier.
   *
   * @param string $value
   *   The Core - Company Code Identifier.
   *
   * @return \Drupal\cdi_companycode\Entity\CdiCompanyCodeInterface
   *   The called Core - Company Code entity.
   */
  public function setIdentifier($value);

  /**
   * Gets the Core - Company Code long description.
   *
   * @return string
   *   Long description of the Core - Company Code.
   */
  public function getLongDesc();

  /**
   * Sets the Core - Company Code long description.
   *
   * @param string $value
   *   The Core - Company Code long description.
   *
   * @return \Drupal\cdi_companycode\Entity\CdiCompanyCodeInterface
   *   The called Core - Company Code entity.
   */
  public function setLongDesc($value);

  /**
   * Gets the Core - Company Code creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Core - Company Code.
   */
  public function getCreatedTime();

  /**
   * Sets the Core - Company Code creation timestamp.
   *
   * @param int $timestamp
   *   The Core - Company Code creation timestamp.
   *
   * @return \Drupal\cdi_companycode\Entity\CdiCompanyCodeInterface
   *   The called Core - Company Code entity.
   */
  public function setCreatedTime($timestamp);

}
