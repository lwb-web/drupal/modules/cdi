<?php

namespace Drupal\cdi_companycode\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Core - Company Code entities.
 */
class CdiCompanyCodeViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
