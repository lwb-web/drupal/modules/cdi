<?php

namespace Drupal\cdi_companycode\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Core - Company Code entities.
 *
 * @ingroup cdi_companycode
 */
class CdiCompanyCodeDeleteForm extends ContentEntityDeleteForm {


}
