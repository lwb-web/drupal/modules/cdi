# Core Data Import -Kostenstelle (cdi_costcenter)

## Features

* Import von Kostenstellen

## Berechtigungen

* add cdi_costcenter entities: Anlegen von Kostenstellen
* administer cdi_costcenter entities: Administration von Kostenstellen
* delete cdi_costcenter entities: Löschen von Kostenstellen
* edit cdi_costcenter entities: Editieren von Kostenstellen
* view published cdi_costcenter entities: Anzeigeberechtigung von veröffentlichten Kostenstellen
* view unpublished cdi_costcenter entities: Anzeigeberechtigung von unveröffentlichten Kostenstellen

## Felder

* ID
* Beschreibung
* Langbeschreibung