<?php

namespace Drupal\cdi_costcenter;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Core - Cost Center entity.
 *
 * @see \Drupal\cdi_costcenter\Entity\CdiCostCenter.
 */
class CdiCostCenterAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\cdi_costcenter\Entity\CdiCostCenterInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cdi_costcenter entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published cdi_costcenter entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit cdi_costcenter entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete cdi_costcenter entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cdi_costcenter entities');
  }

}
