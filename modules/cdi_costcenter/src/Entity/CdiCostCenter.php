<?php

namespace Drupal\cdi_costcenter\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Core - Cost Center entity.
 *
 * @ingroup cdi_costcenter
 *
 * @ContentEntityType(
 *   id = "cdi_costcenter",
 *   label = @Translation("Core - Cost Center"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cdi_costcenter\CdiCostCenterListBuilder",
 *     "views_data" = "Drupal\cdi_costcenter\Entity\CdiCostCenterViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\cdi_costcenter\Form\CdiCostCenterForm",
 *       "add" = "Drupal\cdi_costcenter\Form\CdiCostCenterForm",
 *       "edit" = "Drupal\cdi_costcenter\Form\CdiCostCenterForm",
 *       "delete" = "Drupal\cdi_costcenter\Form\CdiCostCenterDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cdi_costcenter\CdiCostCenterHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\cdi_costcenter\CdiCostCenterAccessControlHandler",
 *   },
 *   base_table = "cdi_costcenter",
 *   translatable = FALSE,
 *   admin_permission = "administer Core - Cost Center entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/cdi_costcenter/{cdi_costcenter}",
 *     "add-form" = "/admin/content/cdi/cdi_costcenter/add",
 *     "edit-form" = "/admin/content/cdi/cdi_costcenter/{cdi_costcenter}/edit",
 *     "delete-form" =
 *   "/admin/content/cdi/cdi_costcenter/{cdi_costcenter}/delete",
 *     "collection" = "/admin/content/cdi/cdi_costcenter",
 *   },
 *   field_ui_base_route = "cdi_costcenter.settings"
 * )
 */
class CdiCostCenter extends ContentEntityBase implements CdiCostCenterInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($value) {
    $this->set('title', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier() {
    return $this->get('core_identifier')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setIdentifier($value) {
    $this->set('core_identifier', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLongDesc() {
    return $this->get('core_longdesc')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLongDesc($value) {
    $this->set('core_longdesc', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Core - Cost Center entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the Core - Cost Center entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['core_identifier'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID'))
      ->setDescription(t('Identifier'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['core_longdesc'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Long description'))
      ->setDescription(t('Long description'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['status']->setDescription(t('A boolean indicating whether the Core - Cost Center is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
