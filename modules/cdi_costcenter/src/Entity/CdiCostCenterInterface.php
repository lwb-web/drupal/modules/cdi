<?php

namespace Drupal\cdi_costcenter\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Core - Cost Center entities.
 *
 * @ingroup cdi_costcenter
 */
interface CdiCostCenterInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Core - Cost Center title.
   *
   * @return string
   *   Title of the Core - Cost Center.
   */
  public function getTitle();

  /**
   * Sets the Core - Cost Center title.
   *
   * @param string $value
   *   The Core - Cost Center title.
   *
   * @return \Drupal\cdi_costcenter\Entity\CdiCostCenterInterface
   *   The called Core - Cost Center entity.
   */
  public function setTitle($value);

  /**
   * Gets the Core - Cost Center Identifier.
   *
   * @return string
   *   Identifier of the Core - Cost Center.
   */
  public function getIdentifier();

  /**
   * Sets the Core - Cost Center Identifier.
   *
   * @param string $value
   *   The Core - Cost Center Identifier.
   *
   * @return \Drupal\cdi_costcenter\Entity\CdiCostCenterInterface
   *   The called Core - Cost Center entity.
   */
  public function setIdentifier($value);

  /**
   * Gets the Core - Cost Center long description.
   *
   * @return string
   *   Long description of the Core - Cost Center.
   */
  public function getLongDesc();

  /**
   * Sets the Core - Cost Center long description.
   *
   * @param string $value
   *   The Core - Cost Center long description.
   *
   * @return \Drupal\cdi_costcenter\Entity\CdiCostCenterInterface
   *   The called Core - Cost Center entity.
   */
  public function setLongDesc($value);

  /**
   * Gets the Core - Cost Center creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Core - Cost Center.
   */
  public function getCreatedTime();

  /**
   * Sets the Core - Cost Center creation timestamp.
   *
   * @param int $timestamp
   *   The Core - Cost Center creation timestamp.
   *
   * @return \Drupal\cdi_costcenter\Entity\CdiCostCenterInterface
   *   The called Core - Cost Center entity.
   */
  public function setCreatedTime($timestamp);

}
