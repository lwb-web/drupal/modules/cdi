<?php

namespace Drupal\cdi_costcenter\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Core - Cost Center entities.
 */
class CdiCostCenterViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
