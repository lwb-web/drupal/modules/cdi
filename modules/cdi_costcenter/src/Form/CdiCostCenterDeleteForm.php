<?php

namespace Drupal\cdi_costcenter\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Core - Cost Center entities.
 *
 * @ingroup cdi_costcenter
 */
class CdiCostCenterDeleteForm extends ContentEntityDeleteForm {


}
