# Core Data Import - Instandhaltungsbezirk (cdi_district)

## Features

* Import von Instandhaltungsbezirken

## Berechtigungen

* add cdi_district entities: Anlegen von Instandhaltungsbezirken
* administer cdi_district entities: Administration von Instandhaltungsbezirken
* delete cdi_district entities: Löschen von Instandhaltungsbezirken
* edit cdi_district entities: Editieren von Instandhaltungsbezirken
* view published cdi_district entities: Anzeigeberechtigung von veröffentlichten Instandhaltungsbezirken
* view unpublished cdi_district entities: Anzeigeberechtigung von unveröffentlichten Instandhaltungsbezirken

## Felder

* ID
* Beschreibung
* Langbeschreibung (ID + Beschreibung)
