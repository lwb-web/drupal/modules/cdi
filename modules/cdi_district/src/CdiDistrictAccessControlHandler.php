<?php

namespace Drupal\cdi_district;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Core - District entity.
 *
 * @see \Drupal\cdi_district\Entity\CdiDistrict.
 */
class CdiDistrictAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\cdi_district\Entity\CdiDistrictInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cdi_district entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published cdi_district entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit cdi_district entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete cdi_district entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cdi_district entities');
  }

}
