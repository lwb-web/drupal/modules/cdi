<?php

namespace Drupal\cdi_district\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Core - District entities.
 *
 * @ingroup cdi_district
 */
interface CdiDistrictInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Core - District title.
   *
   * @return string
   *   Title of the Core - District.
   */
  public function getTitle();

  /**
   * Sets the Core - District title.
   *
   * @param string $value
   *   The Core - District title.
   *
   * @return \Drupal\cdi_district\Entity\CdiDistrictInterface
   *   The called Core - District entity.
   */
  public function setTitle($value);

  /**
   * Gets the Core - District Identifier.
   *
   * @return string
   *   Identifier of the Core - District.
   */
  public function getIdentifier();

  /**
   * Sets the Core - District Identifier.
   *
   * @param string $value
   *   The Core - District Identifier.
   *
   * @return \Drupal\cdi_district\Entity\CdiDistrictInterface
   *   The called Core - District entity.
   */
  public function setIdentifier($value);

  /**
   * Gets the Core - District long description.
   *
   * @return string
   *   Long description of the Core - District.
   */
  public function getLongDesc();

  /**
   * Sets the Core - District long description.
   *
   * @param string $value
   *   The Core - District long description.
   *
   * @return \Drupal\cdi_district\Entity\CdiDistrictInterface
   *   The called Core - District entity.
   */
  public function setLongDesc($value);

  /**
   * Gets the Core - District creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Core - District.
   */
  public function getCreatedTime();

  /**
   * Sets the Core - District creation timestamp.
   *
   * @param int $timestamp
   *   The Core - District creation timestamp.
   *
   * @return \Drupal\cdi_district\Entity\CdiDistrictInterface
   *   The called Core - District entity.
   */
  public function setCreatedTime($timestamp);

}
