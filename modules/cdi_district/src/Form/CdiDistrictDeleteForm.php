<?php

namespace Drupal\cdi_district\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Core - District entities.
 *
 * @ingroup cdi_district
 */
class CdiDistrictDeleteForm extends ContentEntityDeleteForm {


}
