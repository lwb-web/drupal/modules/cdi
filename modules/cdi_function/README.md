# Core Data Import - Funktion (cdi_function)

## Features

* Import von Funktionen

## Berechtigungen

* add cdi_function entities: Anlegen von Funktionen
* administer cdi_function entities: Administration von Funktionen
* delete cdi_function entities: Löschen von Funktionen
* edit cdi_function entities: Editieren von Funktionen
* view published cdi_function entities: Anzeigeberechtigung von veröffentlichten Funktionen
* view unpublished cdi_function entities: Anzeigeberechtigung von unveröffentlichten Funktionen

## Felder

* ID
* Beschreibung
* Langbeschreibung
