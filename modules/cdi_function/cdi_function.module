<?php

/**
 * @file
 * Contains cdi_function.module.
 */

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function cdi_function_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the cdi_function module.
    case 'help.page.cdi_function':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Core Data Importer. Imports function') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function cdi_function_theme($existing, $type, $theme, $path) {
  $theme = [
    'cdi_function' => [
      'file' => 'cdi_function.page.inc',
      'render element' => 'elements',
    ],
  ];
  return $theme;
}

/**
 * Implements hook_field_group_pre_render_alter().
 */
function cdi_function_field_group_pre_render_alter(&$element, $group, &$form) {
  if ($group->bundle === "cdi_function") {
    if ($group->group_name === "group_function_vgroup") {
      $view = views_embed_view('cdi_function_users');
      $element["group_employees_group"]['function_users']['#markup'] = \Drupal::service('renderer')->render($view);
    }
  }
}

/**
 * Create Schema.
 *
 * @return \Drupal\cdi\Cdi\CdiSchema
 *   Return Schema.
 */
function _cdi_function_schema() {
  $schema = new CdiSchema('core', 'functions', 'function');
  $schema->setTable('functions');
  $schema->setFields([
    'longdesc' => 'core_longdesc',
    'id' => 'core_identifier',
  ]);
  $schema->setExpression([
    'title' => '(CASE functions.desc WHEN NULL THEN functions.longdesc ELSE functions.desc END)',
    'uid' => 1
  ]);
  $schema->setWhereExpression([
    'id IN (SELECT function_id FROM employees)',
  ]);
  $schema->setOrderField('id');
  $schema->setCount();
  return $schema;
}
