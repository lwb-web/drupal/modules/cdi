<?php

/**
 * @file
 * Contains cdi_function.page.inc.
 *
 * Page callback for Core - Function entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Core - Function templates.
 *
 * Default template: cdi-function.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cdi_function(array &$variables) {
  // Fetch CdiFunction Entity Object.
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
  $localTasksBlock = \Drupal::service('plugin.manager.block')->createInstance('local_tasks_block', []);
  $variables['local_tasks'] = $localTasksBlock->build();
}
