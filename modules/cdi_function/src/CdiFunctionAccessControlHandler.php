<?php

namespace Drupal\cdi_function;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Core - Function entity.
 *
 * @see \Drupal\cdi_function\Entity\CdiFunction.
 */
class CdiFunctionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\cdi_function\Entity\CdiFunctionInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cdi_function entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published cdi_function entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit cdi_function entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete cdi_function entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cdi_function entities');
  }

}
