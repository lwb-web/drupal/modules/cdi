<?php

namespace Drupal\cdi_function\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Core - Function entities.
 *
 * @ingroup cdi_function
 */
interface CdiFunctionInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Core - Function title.
   *
   * @return string
   *   Title of the Core - Function.
   */
  public function getTitle();

  /**
   * Sets the Core - Function title.
   *
   * @param string $value
   *   The Core - Function title.
   *
   * @return \Drupal\cdi_function\Entity\CdiFunctionInterface
   *   The called Core - Function entity.
   */
  public function setTitle($value);

  /**
   * Gets the Core - Function Identifier.
   *
   * @return string
   *   Identifier of the Core - Function.
   */
  public function getIdentifier();

  /**
   * Sets the Core - Function Identifier.
   *
   * @param string $value
   *   The Core - Function Identifier.
   *
   * @return \Drupal\cdi_function\Entity\CdiFunctionInterface
   *   The called Core - Function entity.
   */
  public function setIdentifier($value);

  /**
   * Gets the Core - Function long description.
   *
   * @return string
   *   Long description of the Core - Function.
   */
  public function getLongDesc();

  /**
   * Sets the Core - Function long description.
   *
   * @param string $value
   *   The Core - Function long description.
   *
   * @return \Drupal\cdi_function\Entity\CdiFunctionInterface
   *   The called Core - Function entity.
   */
  public function setLongDesc($value);

  /**
   * Gets the Core - Function creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Core - Function.
   */
  public function getCreatedTime();

  /**
   * Sets the Core - Function creation timestamp.
   *
   * @param int $timestamp
   *   The Core - Function creation timestamp.
   *
   * @return \Drupal\cdi_function\Entity\CdiFunctionInterface
   *   The called Core - Function entity.
   */
  public function setCreatedTime($timestamp);

}
