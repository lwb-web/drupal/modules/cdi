<?php

namespace Drupal\cdi_function\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Core - Function entities.
 */
class CdiFunctionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
