<?php

namespace Drupal\cdi_function\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Core - Function entities.
 *
 * @ingroup cdi_function
 */
class CdiFunctionDeleteForm extends ContentEntityDeleteForm {


}
