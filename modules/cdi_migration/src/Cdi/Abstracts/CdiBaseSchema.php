<?php


namespace Drupal\cdi_migration\Cdi\Abstracts;

abstract class CdiBaseSchema extends NodeSchema {

  /**
   * CdiBaseSchema constructor.
   *
   * @param string $name
   * @param string $type
   */
  public function __construct(string $name, string $type) {
    parent::__construct($name, $type);
    $this->set_cdi_defaults();
  }

  private function set_cdi_defaults() {
    $this->schema->fields[] = 'title';
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_identifier',
        'alias' => 'identifier',
        'fields' => ['field_identifier_value' => 'core_identifier'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_longdesc',
        'alias' => 'longdesc',
        'fields' => ['field_longdesc_value' => 'core_longdesc'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ]);
    $this->schema->setOrderField('n.nid');
    $this->schema->setConditions([
      'n.type',
      $this->schema->name,
    ],[
      'identifier.field_identifier_value', '', '<>'
    ]);
  }
}
