<?php


namespace Drupal\cdi_migration\Cdi\Abstracts;


use Drupal\cdi\Cdi\CdiSchema;

abstract class CdiMigrationSchema {

  /**
   * @var \Drupal\cdi\Cdi\CdiSchema
   */
  protected CdiSchema $schema;

  /**
   * CdiMigrationSchema constructor.
   *
   * @param string $name
   * @param string $type
   */
  public function __construct(string $name, string $type) {
    $this->schema = new CdiSchema('d7', $name, $type);
  }

  abstract public function get_schema(): CdiSchema;

}
