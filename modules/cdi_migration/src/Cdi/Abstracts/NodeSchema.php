<?php


namespace Drupal\cdi_migration\Cdi\Abstracts;


abstract class NodeSchema extends CdiMigrationSchema {

  public function __construct(string $name, string $type) {
    parent::__construct($name, $type);
    $this->set_node_defaults();
  }

  private function set_node_defaults() {
    $this->schema->setAlias('n');
    $this->schema->setTable('node');
    $this->schema->setFields([
      'nid',
      'nid' => 'id',
      'uid',
      'status',
      'created',
      'type',
    ]);
    $this->schema->setOrderField('n.nid');
    $this->schema->setConditions([
      'n.type',
      $this->schema->name,
    ]);
  }

}
