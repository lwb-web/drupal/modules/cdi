<?php

namespace Drupal\cdi_migration\Cdi;

class CdiMigration {

  public static function get_schema($name) {
    switch ($name) {
      // Contenttypes
      case 'grundbuchblatt':
        $schema = new GrundbuchblattSchema();
        break;
      case 'pdb_bericht':
        $schema = new PDBBerichtSchema();
        break;
      case 'rdb_text':
        $schema = new RDBTextSchema();
        break;
      case 'rdb_person':
        $schema = new RDBPersonSchema();
        break;
      case 'rdb_process':
        $schema = new RDBProcessSchema();
        break;
      case 'rdb_instance':
        $schema = new RDBInstanceSchema();
        break;
      case 'gutachten':
        $schema = new VDBGutachten();
        break;
      case 'musterschreiben':
        $schema = new VDBMusterschreiben();
        break;
      case 'vertragsentwurf':
        $schema = new VDBVertragsentwurf();
        break;
      case 'gesellschaftsvertrag':
        $schema = new VDBGesellschaftsvertrag();
        break;
      case 'vdb_contract':
        $schema = new VDBVertrag();
        break;
      case 'vdb_limit':
        $schema = new VDBFristSchema();
        break;
      // Cdi's
      case 'costcenter':
        $schema = new CostCenterSchema();
        break;
      case 'companycode':
        $schema = new CompanyCodeSchema();
        break;
      case 'district':
        $schema = new DistrictSchema();
        break;
      case 'organisationalunit':
        $schema = new OrganisationalUnitSchema();
        break;
      case 'function':
        $schema = new FunctionSchema();
        break;
      //Drupal Defaults
      case 'files':
        $schema = new FilesSchema();
        break;
      case 'terms':
        $schema = new TermsSchema();
        break;
      case 'user':
        $schema = new UserSchema();
        break;
      default:
        return FALSE;
    }
    return $schema->get_schema();
  }

}
