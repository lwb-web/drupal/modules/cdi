<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\CdiBaseSchema;

class CompanyCodeSchema extends CdiBaseSchema {

  /**
   * FunctionSchema constructor.
   */
  public function __construct() {
    parent::__construct('companycode', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->setToType('companycode');
    $this->schema->setCount();
    return $this->schema;
  }

}
