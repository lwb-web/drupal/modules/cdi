<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\CdiBaseSchema;

class CostCenterSchema extends CdiBaseSchema {

  /**
   * CostCenterSchema constructor.
   */
  public function __construct() {
    parent::__construct('costcenter', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->setToType('costcenter');
    $this->schema->setCount();
    return $this->schema;
  }

}
