<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\CdiBaseSchema;

class DistrictSchema extends CdiBaseSchema {

  /**
   * FunctionSchema constructor.
   */
  public function __construct() {
    parent::__construct('district', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->setToType('district');
    $this->schema->setCount();
    return $this->schema;
  }

}
