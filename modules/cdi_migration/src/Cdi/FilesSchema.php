<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\CdiMigrationSchema;

class FilesSchema extends CdiMigrationSchema {

  /**
   * FilesSchema constructor.
   */
  public function __construct() {
    parent::__construct('file_managed', 'file');
  }

  public function get_schema(): CdiSchema {
    $this->schema->limit = 400;
    $this->schema->setTable('file_managed');
    $this->schema->setAlias('files');
    $this->schema->setFields([
      'fid',
      'fid' => 'id',
      'uid',
      'filename',
      'filemime',
      'uri',
      'filesize',
      'status',
      'timestamp' => 'created',
    ]);
    $this->schema->setOrderField('files.fid');
    $this->schema->setCount();
    return $this->schema;
  }

}
