<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;

class GrundbuchblattSchema extends NodeSchema {

  /**
   * GrundbuchblattSchema constructor.
   */
  public function __construct() {
    parent::__construct('grundbuchblatt', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_anlage',
        'alias' => 'anlage',
        'fields' => ['field_anlage_fid' => 'field_anlage'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_body',
        'alias' => 'body',
        'fields' => ['body_value' => 'body'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_blattnummer',
        'alias' => 'blattnummer',
        'fields' => ['field_blattnummer_value' => 'title'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_flurstuecksnummer',
        'alias' => 'flurstuecksnummer',
        'fields' => ['field_flurstuecksnummer_value' => 'field_flurstuecksnummer'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_gemarkung',
        'alias' => 'gemarkung',
        'fields' => ['field_gemarkung_tid' => 'field_gemarkung'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_gezogen_am',
        'alias' => 'gezogen_am',
        'fields' => ['field_gezogen_am_value' => 'field_gezogen_am'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_gezogen_am' => 'CAST(`field_gezogen_am_value` AS date)'],
      ],
      [
        'table' => 'field_data_field_kostentraeger',
        'alias' => 'kostentraeger',
        'fields' => ['field_kostentraeger_value' => 'field_kostentraeger'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_objektbezeichnung',
        'alias' => 'objektbezeichnung',
        'fields' => ['field_objektbezeichnung_value' => 'field_objektbezeichnung'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ]
    );
    $this->schema->setCount();
    return $this->schema;
  }

}
