<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\CdiBaseSchema;

class OrganisationalUnitSchema extends CdiBaseSchema {

  /**
   * OrganisationalUnitSchema constructor.
   */
  public function __construct() {
    parent::__construct('organisationalunit', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->setToType('organisationalunit');
    $this->schema->setCount();
    return $this->schema;
  }

}
