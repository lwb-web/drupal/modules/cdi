<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;

class PDBBerichtSchema extends NodeSchema {

  /**
   * PDBBerichtSchema constructor.
   */
  public function __construct() {
    parent::__construct('pdb_bericht', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->fields[] = 'title';
    $this->schema->setGroupBy(TRUE);
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_pdb_taxonomy',
        'alias' => 'pdb_taxonomy',
        'fields' => ['field_pdb_taxonomy_tid' => 'field_pdb_taxonomy'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_pdb_taxonomy' => 'GROUP_CONCAT(DISTINCT field_pdb_taxonomy_tid SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_documents',
        'alias' => 'documents',
        'fields' => [
          'field_documents_fid' => 'field_documents',
          'field_documents_description',
        ],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_documents' => 'GROUP_CONCAT(DISTINCT CONCAT(field_documents_fid, \'=>\', field_documents_description) SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_body',
        'alias' => 'body',
        'fields' => ['body_value' => 'body'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_pdb_department',
        'alias' => 'pdb_department',
        'fields' => ['field_pdb_department_value' => 'field_pdb_department'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_pdb_number',
        'alias' => 'pdb_number',
        'fields' => ['field_pdb_number_value' => 'field_pdb_number'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_pdb_date',
        'alias' => 'pdb_date',
        'fields' => ['field_pdb_date_value' => 'field_pdb_date'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid'
      ],
    );
    $this->schema->setCount();
    $this->schema->setBeforeInsert([self::class, 'prepareData']);
    return $this->schema;
  }

  /**
   * @param $data
   *
   * @return \stdClass
   */
  public static function prepareData($data) {
    if ($data->field_pdb_date) {
      $data->field_pdb_date = date("Y-m-d", strtotime($data->field_pdb_date));
    }
    if ($data->field_pdb_taxonomy) {
      if (is_string($data->field_pdb_taxonomy)) {
        $data->field_pdb_taxonomy = explode(' | ', $data->field_pdb_taxonomy);
      }
    }
    if ($data->field_documents) {
      if (is_string($data->field_documents)) {
        $files = [];
        foreach (explode(' | ', $data->field_documents) as $file) {
          $f = explode('=>', $file);
          $files[] = [
            'target_id' => $f[0],
            'description' => $f[1],
          ];
        }
        $data->field_documents = $files;
      }
    }
    return $data;
  }

}
