<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;
use Drupal\node\Entity\Node;

class RDBInstanceSchema extends NodeSchema {

  /**
   * RDBInstanceSchema constructor.
   */
  public function __construct() {
    parent::__construct('rdb_instance', 'node');
  }

  /**
   * @return \Drupal\cdi\Cdi\CdiSchema
   *
   */
  public function get_schema(): CdiSchema {
    $this->schema->limit = 400;
    $this->schema->setToType('rdb_instance');
    $this->schema->fields['title'] = 'field_rdb_instance_az_gericht';
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_rdb_instance',
        'alias' => 'field_rdb_instance',
        'fields' => ['field_rdb_instance_value' => 'field_rdb_instance'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ], [
      'table' => 'field_data_field_rdb_instance_court',
      'alias' => 'field_rdb_instance_court',
      'fields' => ['field_rdb_instance_court_nid' => 'field_rdb_instance_court'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_instance_jurist',
      'alias' => 'field_rdb_instance_jurist',
      'fields' => ['field_rdb_instance_jurist_nid' => 'field_rdb_instance_jurist'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_instance_titel',
      'alias' => 'field_rdb_instance_titel',
      'fields' => ['field_rdb_instance_titel_nid' => 'field_rdb_instance_titel'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
        'table' => 'field_data_field_rdb_instance_process',
        'alias' => 'instance_process_nid',
        'fields' => ['field_rdb_instance_process_nid' => 'instance_process_nid'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ]
    );
    $this->schema->setCount();
    $this->schema->setAfterInsert([self::class, 'afterInsert']);
    return $this->schema;
  }

  /**
   * @param $entity
   * @param $insertedEntity
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function afterInsert($entity, $insertedEntity) {
    if(!empty($entity->instance_process_nid)) {
      $node = Node::load($entity->instance_process_nid);
      if ($node) {
        $references = $node->get('field_rdb_process_instance')
          ->referencedEntities();
        $instances = [];
        foreach ($references as $reference) {
          $instances[] = [
            'target_id' => $reference->id(),
            'target_revision_id' => $reference->getRevisionId(),
          ];
        }
        $newInstance = [
          'target_id' => $insertedEntity->id(),
          'target_revision_id' => $insertedEntity->getRevisionId(),
        ];
        if (!in_array($newInstance, $instances)) {
          $instances[] = $newInstance;
        }
        $node->set('field_rdb_process_instance', $instances);
        $node->save();
      }
    }
  }

}
