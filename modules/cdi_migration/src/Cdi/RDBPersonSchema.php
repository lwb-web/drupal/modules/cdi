<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;

class RDBPersonSchema extends NodeSchema {

  /**
   * RDBPersonSchema constructor.
   */
  public function __construct() {
    parent::__construct('rdb_person', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->fields[] = 'title';
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_rdb_person_surname',
        'alias' => 'rdb_person_surname',
        'fields' => ['field_rdb_person_surname_value' => 'field_rdb_person_surname'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
      'table' => 'field_data_field_rdb_person_name',
        'alias' => 'rdb_person_name',
        'fields' => ['field_rdb_person_name_value' => 'field_rdb_person_name'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_rdb_person_birthday',
        'alias' => 'rdb_person_birthday',
        'fields' => ['field_rdb_person_birthday_value' => 'field_rdb_person_birthday'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_rdb_person_birthday' => 'CAST(`field_rdb_person_birthday_value` AS date)'],
      ],
      [
        'table' => 'field_data_field_rdb_person_phone',
        'alias' => 'rdb_person_phone',
        'fields' => ['field_rdb_person_phone_value' => 'field_rdb_person_phone'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_rdb_person_email',
        'alias' => 'rdb_person_email',
        'fields' => ['field_rdb_person_email_email' => 'field_rdb_person_email'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_rdb_person_category',
        'alias' => 'rdb_person_category',
        'fields' => ['field_rdb_person_category_value' => 'field_rdb_person_category'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ]
    );
    $this->schema->setCount();
    return $this->schema;
  }

  /**
   * @param $data
   *
   * @return \stdClass
   */
  public static function prepareData($data) {
    return $data;
  }

}
