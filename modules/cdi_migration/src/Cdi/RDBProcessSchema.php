<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;

class RDBProcessSchema extends NodeSchema {

  /**
   * RDBProcessSchema constructor.
   */
  public function __construct() {
    parent::__construct('rdb_process', 'node');
  }

  /**
   * @return \Drupal\cdi\Cdi\CdiSchema
   *
   */
  public function get_schema(): CdiSchema {
    $this->schema->limit = 800;
    $this->schema->fields[] = 'title';
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_rdb_process_abgabe_archiv',
        'alias' => 'rdb_process_abgabe_archiv',
        'fields' => ['field_rdb_process_abgabe_archiv_value' => 'field_rdb_process_abgabe_archiv'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_rdb_process_abgabe_archiv' => 'CAST(`field_rdb_process_abgabe_archiv_value` AS date)'],
      ], [
      'table' => 'field_data_field_rdb_process_anmerkung',
      'alias' => 'rdb_process_anmerkung',
      'fields' => ['field_rdb_process_anmerkung_value' => 'field_rdb_process_anmerkung'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_person2',
      'alias' => 'rdb_process_person2',
      'fields' => ['field_rdb_process_person2_nid' => 'field_rdb_process_person2'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_betreff',
      'alias' => 'rdb_process_betreff',
      'fields' => ['field_rdb_process_betreff_nid' => 'field_rdb_process_betreff'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_companycode',
      'alias' => 'companycode',
      'fields' => ['field_companycode_nid' => 'field_companycode'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_mand',
      'alias' => 'rdb_process_mand',
      'fields' => ['field_rdb_process_mand_value' => 'field_rdb_process_mand'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_folnr',
      'alias' => 'rdb_process_folnr',
      'fields' => ['field_rdb_process_folnr_value' => 'field_rdb_process_folnr'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_gerichtskosten',
      'alias' => 'rdb_process_gerichtskosten',
      'fields' => ['field_rdb_process_gerichtskosten_value' => 'field_rdb_process_gerichtskosten'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_group',
      'alias' => 'rdb_process_group',
      'fields' => ['field_rdb_process_group_value' => 'field_rdb_process_group'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_district',
      'alias' => 'district',
      'fields' => ['field_district_nid' => 'field_district'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_person3',
      'alias' => 'rdb_process_person3',
      'fields' => ['field_rdb_process_person3_uid' => 'field_rdb_process_person3'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_klageart',
      'alias' => 'rdb_process_klageart',
      'fields' => ['field_rdb_process_klageart_nid' => 'field_rdb_process_klageart'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_lfdnr',
      'alias' => 'rdb_process_lfdnr',
      'fields' => ['field_rdb_process_lfdnr_value' => 'field_rdb_process_lfdnr'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_mn',
      'alias' => 'rdb_process_mn',
      'fields' => ['field_rdb_process_mn_value' => 'field_rdb_process_mn'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_nuart',
      'alias' => 'rdb_process_nuart',
      'fields' => ['field_rdb_process_nuart_value' => 'field_rdb_process_nuart'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_rubrum',
      'alias' => 'rdb_process_rubrum',
      'fields' => ['field_rdb_process_rubrum_value' => 'field_rdb_process_rubrum'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_category',
      'alias' => 'rdb_process_category',
      'fields' => ['field_rdb_process_category_value' => 'field_rdb_process_category'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_street',
      'alias' => 'rdb_process_street',
      'fields' => ['field_rdb_process_street_value' => 'field_rdb_process_street'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_streitwert',
      'alias' => 'rdb_process_streitwert',
      'fields' => ['field_rdb_process_streitwert_value' => 'field_rdb_process_streitwert'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_rdb_process_venr',
      'alias' => 'rdb_process_venr',
      'fields' => ['field_rdb_process_venr_value' => 'field_rdb_process_venr'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
      'table' => 'field_data_field_wkz',
      'alias' => 'wkz',
      'fields' => ['field_wkz_value' => 'field_wkz'],
      'tableId' => 'entity_id',
      'joinTable' => 'n',
      'join_id' => 'nid',
    ], [
        'table' => 'field_data_field_rdb_process_stempeldatum',
        'alias' => 'rdb_process_stempeldatum',
        'fields' => ['field_rdb_process_stempeldatum_value' => 'field_rdb_process_stempeldatum'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_rdb_process_stempeldatum' => 'CAST(`field_rdb_process_stempeldatum_value` AS date)'],
      ]
    );
    $this->schema->setCount();
    return $this->schema;
  }

}
