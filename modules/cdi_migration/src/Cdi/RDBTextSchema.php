<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;

class RDBTextSchema extends NodeSchema {

  /**
   * RDBTextSchema constructor.
   */
  public function __construct() {
    parent::__construct('rdb_text', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->fields[] = 'title';
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_rdb_text_category',
        'alias' => 'rdb_text_category',
        'fields' => ['field_rdb_text_category_value' => 'field_rdb_text_category'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ]
    );
    $this->schema->setCount();
    return $this->schema;
  }

  /**
   * @param $data
   *
   * @return \stdClass
   */
  public static function prepareData($data) {
    return $data;
  }

}
