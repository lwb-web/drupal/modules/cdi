<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\CdiMigrationSchema;

class TermsSchema extends CdiMigrationSchema {

  /**
   * TermsSchema constructor.
   */
  public function __construct() {
    parent::__construct('term', 'term');
  }

  public function get_schema(): CdiSchema {
    $this->schema->setTable('taxonomy_term_data');
    $this->schema->setAlias('term');
    $this->schema->setFields(['tid', 'tid' => 'id', 'name']);
    $this->schema->setJoinFields([
      'table' => 'taxonomy_vocabulary',
      'fields' => ['machine_name' => 'vid'],
      'tableId' => 'vid',
      'joinTable' => 'term',
      'join_id' => 'vid',
    ]);
    $this->schema->setOrderField('term.tid');
    $this->schema->setCount();
    return $this->schema;
  }

}
