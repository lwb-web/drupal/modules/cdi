<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\CdiMigrationSchema;

class UserSchema extends CdiMigrationSchema {

  /**
   * UserSchema constructor.
   */
  public function __construct() {
    parent::__construct('user', 'user');
  }

  public function get_schema(): CdiSchema {
    $this->schema->setAlias('u');
    $this->schema->setToType('user');
    $this->schema->setTable('users');
    $this->schema->setGroupBy(TRUE);
    $this->schema->setFields([
      'uid',
      'uid' => 'id',
      'name',
      'mail',
      'created',
      'status',
    ]);
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_email',
        'alias' => 'field_email',
        'fields' => ['field_email_email' => 'field_email'],
        'tableId' => 'entity_id',
        'joinTable' => 'u',
        'join_id' => 'uid',
      ], [
      'table' => 'field_data_field_fax',
      'alias' => 'field_fax',
      'fields' => ['field_fax_value' => 'field_fax'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'field_data_field_login',
      'alias' => 'field_login',
      'fields' => ['field_login_value' => 'field_login'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'field_data_field_name',
      'alias' => 'field_name',
      'fields' => ['field_name_value' => 'field_name'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'field_data_field_room',
      'alias' => 'field_room',
      'fields' => ['field_room_value' => 'field_room'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'field_data_field_is_active',
      'alias' => 'field_is_active',
      'fields' => ['field_is_active_value' => 'field_is_active'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'field_data_field_membership',
      'alias' => 'field_membership',
      'fields' => ['field_membership_value' => 'field_membership'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'field_data_field_function',
      'alias' => 'field_function',
      'fields' => ['field_function_nid' => 'field_function'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'field_data_field_costcenter',
      'alias' => 'field_costcenter',
      'fields' => ['field_costcenter_nid' => 'field_costcenter'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'field_data_field_organisationalunit',
      'alias' => 'field_organisationalunit',
      'fields' => ['field_organisationalunit_nid' => 'field_organisationalunit'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'field_data_field_phone',
      'alias' => 'field_phone',
      'fields' => ['field_phone_value' => 'field_phone'],
      'tableId' => 'entity_id',
      'joinTable' => 'u',
      'join_id' => 'uid',
      'expression' => ['field_phone' => 'GROUP_CONCAT(DISTINCT field_phone_value SEPARATOR \' | \')'],
    ], [
      'table' => 'users_roles',
      'alias' => 'uroles',
      'fields' => [],
      'tableId' => 'uid',
      'joinTable' => 'u',
      'join_id' => 'uid',
    ], [
      'table' => 'role',
      'alias' => 'r',
      'fields' => ['name' => 'roles'],
      'tableId' => 'rid',
      'joinTable' => 'uroles',
      'join_id' => 'rid',
      'expression' => ['roles' => 'GROUP_CONCAT(DISTINCT LOWER(r.name) SEPARATOR \' | \')'],
    ]);
    $this->schema->setOrderField('u.uid');
    $this->schema->setConditions([
      'u.uid',
      0,
      '!=',
    ]);
    $this->schema->setCount();
    $this->schema->setBeforeInsert([self::class, 'prepareData']);
    return $this->schema;
  }

  /**
   * @param $data
   *
   * @return \stdClass
   */
  public static function prepareData($data) {
    if ($data->field_phone) {
      if(is_string ($data->field_phone)) $data->field_phone = explode(' | ', $data->field_phone);
    }
    if ($data->roles) {
      if(is_string ($data->roles)) $data->roles = explode(' | ', $data->roles);
      foreach ($data->roles AS $key => $role){
        $data->roles[$key] = str_replace([' ', '(', ')', '-', '/'], ['', '', '', '_', '_'], $role);
        if($data->roles[$key] == 'autorohneemail') $data->roles[$key] = 'autor_ohne_email';
      }
    }
    return $data;
  }

}
