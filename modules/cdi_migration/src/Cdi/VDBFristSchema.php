<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;
use Drupal\node\Entity\Node;

class VDBFristSchema extends NodeSchema {

  /**
   * RDBInstanceSchema constructor.
   */
  public function __construct() {
    parent::__construct('vdb_limit', 'node');
  }

  /**
   * @return \Drupal\cdi\Cdi\CdiSchema
   *
   */
  public function get_schema(): CdiSchema {
    $this->schema->limit = 200;
    $this->schema->setToType('vdb_limit');
    $this->schema->fields['title'] = 'field_vdb_limit_title';
    $this->schema->setGroupBy(TRUE);
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_vdb_limit_date',
        'alias' => 'vdb_limit_date',
        'fields' => ['field_vdb_limit_date_value' => 'field_vdb_limit_date'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid'
      ],
      [
        'table' => 'field_data_field_vdb_limit_next_date',
        'alias' => 'vdb_limit_next_date',
        'fields' => ['field_vdb_limit_next_date_value' => 'field_vdb_limit_next_date'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_vdb_limit_next_date' => 'GROUP_CONCAT(DISTINCT field_vdb_limit_next_date_value SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_vdb_limit_interval',
        'alias' => 'vdb_limit_interval',
        'fields' => ['field_vdb_limit_interval_value' => 'field_vdb_limit_interval'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_limit_interval_unit',
        'alias' => 'vdb_limit_interval_unit',
        'fields' => ['field_vdb_limit_interval_unit_value' => 'field_vdb_limit_interval_unit'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_limit_lead_time',
        'alias' => 'vdb_limit_lead_time',
        'fields' => ['field_vdb_limit_lead_time_value' => 'field_vdb_limit_lead_time'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_limit_lead_time_unit',
        'alias' => 'vdb_limit_lead_time_unit',
        'fields' => ['field_vdb_limit_lead_time_unit_value' => 'field_vdb_limit_lead_time_unit'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_limit_contract',
        'alias' => 'vdb_limit_contract_nid',
        'fields' => ['field_vdb_limit_contract_nid' => 'vdb_limit_contract_nid'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_organisationseinheiten',
        'alias' => 'organisationseinheiten',
        'fields' => ['field_organisationseinheiten_target_id' => 'field_organisationseinheiten'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_organisationseinheiten' => 'GROUP_CONCAT(DISTINCT field_organisationseinheiten_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_kostenstellen',
        'alias' => 'kostenstellen',
        'fields' => ['field_kostenstellen_target_id' => 'field_kostenstellen'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_kostenstellen' => 'GROUP_CONCAT(DISTINCT field_kostenstellen_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_funktionen',
        'alias' => 'funktionen',
        'fields' => ['field_funktionen_target_id' => 'field_funktionen'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_funktionen' => 'GROUP_CONCAT(DISTINCT field_funktionen_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_benutzer',
        'alias' => 'benutzer',
        'fields' => ['field_benutzer_target_id' => 'field_benutzer'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_benutzer' => 'GROUP_CONCAT(DISTINCT field_benutzer_target_id SEPARATOR \' | \')'],
      ]
    );
    $this->schema->setCount();
    $this->schema->setBeforeInsert([self::class, 'prepareData']);
    $this->schema->setAfterInsert([self::class, 'afterInsert']);
    return $this->schema;
  }

  /**
   * @param $data
   *
   * @return \stdClass
   */
  public static function prepareData($data) {
    if ($data->field_vdb_limit_date) {
      if(date("Y-m-d", strtotime($data->field_vdb_limit_date)) !== '1970-01-01'){
        $data->field_vdb_limit_date = date("Y-m-d", strtotime($data->field_vdb_limit_date));
      } else{
        unset($data->field_vdb_limit_date);
      }
    }

    if ($data->field_vdb_limit_next_date) {
      if (is_string($data->field_vdb_limit_next_date)) {
        $data->field_vdb_limit_next_date = explode(' | ', $data->field_vdb_limit_next_date);
        foreach ($data->field_vdb_limit_next_date AS $key => $val){
          if(date("Y-m-d", strtotime($val)) !== '1970-01-01'){
            $data->field_vdb_limit_next_date[$key] = date("Y-m-d", strtotime($val));
          } else{
            unset($data->field_vdb_limit_next_date[$key]);
          }
        }
      }
    }

    if ($data->field_organisationseinheiten) {
      if (is_string($data->field_organisationseinheiten)) {
        $data->field_organisationseinheiten = explode(' | ', $data->field_organisationseinheiten);
      }
    }
    if ($data->field_kostenstellen) {
      if (is_string($data->field_kostenstellen)) {
        $data->field_kostenstellen = explode(' | ', $data->field_kostenstellen);
      }
    }
    if ($data->field_funktionen) {
      if (is_string($data->field_funktionen)) {
        $data->field_funktionen = explode(' | ', $data->field_funktionen);
      }
    }
    if ($data->field_benutzer) {
      if (is_string($data->field_benutzer)) {
        $data->field_benutzer = explode(' | ', $data->field_benutzer);
      }
    }
    return $data;
  }

  /**
   * @param $entity
   * @param $insertedEntity
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function afterInsert($entity, $insertedEntity) {

    if(!empty($entity->vdb_limit_contract_nid)) {
      $node = Node::load($entity->vdb_limit_contract_nid);
      if ($node) {
        $references = $node->get('field_vdb_limit')
          ->referencedEntities();
        $limits = [];
        foreach ($references as $reference) {
          $limits[] = [
            'target_id' => $reference->id(),
            'target_revision_id' => $reference->getRevisionId(),
          ];
        }
        $newLimit = [
          'target_id' => $insertedEntity->id(),
          'target_revision_id' => $insertedEntity->getRevisionId(),
        ];
        if (!in_array($newLimit, $limits)) {
          $limits[] = $newLimit;
        }
        $node->set('field_vdb_limit', $limits);
        $node->save();
      }
    }
  }

}
