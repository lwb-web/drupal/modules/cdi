<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;

class VDBGesellschaftsvertrag extends NodeSchema {

  /**
   * VDBGesellschaftsvertrag constructor.
   */
  public function __construct() {
    parent::__construct('gesellschaftsvertrag', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->fields[] = 'title';
    $this->schema->setGroupBy(TRUE);
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_name_gesellschaft_fonds',
        'alias' => 'name_gesellschaft_fonds',
        'fields' => ['field_name_gesellschaft_fonds_value' => 'field_name_gesellschaft_fonds'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_rechtsform',
        'alias' => 'rechtsform',
        'fields' => ['field_rechtsform_value' => 'field_rechtsform'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_gr_ndung',
        'alias' => 'gr_ndung',
        'fields' => ['field_gr_ndung_value' => 'field_gr_ndung'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_handelsregister',
        'alias' => 'handelsregister',
        'fields' => ['field_handelsregister_value' => 'field_handelsregister'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_gesellschaftszweck',
        'alias' => 'gesellschaftszweck',
        'fields' => ['field_gesellschaftszweck_value' => 'field_gesellschaftszweck'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_gmbh_gesellschaft_stammkap',
        'alias' => 'gmbh_gesellschaft_stammkap',
        'fields' => ['field_gmbh_gesellschaft_stammkap_value' => 'field_gmbh_gesellschaft_stammkap'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_kommanditist_einlage',
        'alias' => 'kommanditist_einlage',
        'fields' => ['field_kommanditist_einlage_value' => 'field_kommanditist_einlage'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_komplement_r',
        'alias' => 'komplement_r',
        'fields' => ['field_komplement_r_value' => 'field_komplement_r'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_atypisch_stiller_gesellsch',
        'alias' => 'atypisch_stiller_gesellsch',
        'fields' => ['field_atypisch_stiller_gesellsch_value' => 'field_atypisch_stiller_gesellsch'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_beteiligungen',
        'alias' => 'beteiligungen',
        'fields' => ['field_beteiligungen_value' => 'field_beteiligungen'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_wichtige_vertr_ge',
        'alias' => 'wichtige_vertr_ge',
        'fields' => ['field_wichtige_vertr_ge_value' => 'field_wichtige_vertr_ge'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_satzung',
        'alias' => 'satzung',
        'fields' => ['field_satzung_value' => 'field_satzung'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_gesellschaftsversammlung',
        'alias' => 'gesellschaftsversammlung',
        'fields' => ['field_gesellschaftsversammlung_value' => 'field_gesellschaftsversammlung'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_beschlussfassung_umlaufbef',
        'alias' => 'beschlussfassung_umlaufbef',
        'fields' => ['field_beschlussfassung_umlaufbef_value' => 'field_beschlussfassung_umlaufbef'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_sonstiges',
        'alias' => 'sonstiges',
        'fields' => ['field_sonstiges_value' => 'field_sonstiges'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_documents',
        'alias' => 'documents',
        'fields' => [
          'field_documents_fid' => 'field_documents',
          'field_documents_description',
        ],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_documents' => 'GROUP_CONCAT(DISTINCT CONCAT(field_documents_fid, \'=>\', field_documents_description) SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_organisationseinheiten',
        'alias' => 'organisationseinheiten',
        'fields' => ['field_organisationseinheiten_target_id' => 'field_organisationseinheiten'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_organisationseinheiten' => 'GROUP_CONCAT(DISTINCT field_organisationseinheiten_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_kostenstellen',
        'alias' => 'kostenstellen',
        'fields' => ['field_kostenstellen_target_id' => 'field_kostenstellen'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_kostenstellen' => 'GROUP_CONCAT(DISTINCT field_kostenstellen_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_funktionen',
        'alias' => 'funktionen',
        'fields' => ['field_funktionen_target_id' => 'field_funktionen'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_funktionen' => 'GROUP_CONCAT(DISTINCT field_funktionen_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_benutzer',
        'alias' => 'benutzer',
        'fields' => ['field_benutzer_target_id' => 'field_benutzer'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_benutzer' => 'GROUP_CONCAT(DISTINCT field_benutzer_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_vdb_contract_runtime',
        'alias' => 'vdb_contract_runtime',
        'fields' => [
          'field_vdb_contract_runtime_value' => 'field_vdb_contract_runtime_value',
          'field_vdb_contract_runtime_value2' => 'field_vdb_contract_runtime_end_value',
        ],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid'
      ]
    );
    $this->schema->setCount();
    $this->schema->setBeforeInsert([self::class, 'prepareData']);
    return $this->schema;
  }

  /**
   * @param $data
   *
   * @return \stdClass
   */
  public static function prepareData($data) {
    if ($data->field_documents) {
      if (is_string($data->field_documents)) {
        $files = [];
        foreach (explode(' | ', $data->field_documents) as $file) {
          $f = explode('=>', $file);
          $files[] = [
            'target_id' => $f[0],
            'description' => $f[1],
          ];
        }
        $data->field_documents = $files;
      }
    }
    if ($data->field_vdb_contract_runtime_value) {
      $data->field_vdb_contract_runtime = [
        'value'=> date("Y-m-d", strtotime($data->field_vdb_contract_runtime_value))
      ];
      unset($data->field_vdb_contract_runtime_value);
      if ($data->field_vdb_contract_runtime_end_value) {
        $data->field_vdb_contract_runtime['end_value'] = date("Y-m-d", strtotime($data->field_vdb_contract_runtime_end_value));
        unset($data->field_vdb_contract_runtime_end_value);
      }
    }
    if ($data->field_organisationseinheiten) {
      if (is_string($data->field_organisationseinheiten)) {
        $data->field_organisationseinheiten = explode(' | ', $data->field_organisationseinheiten);
      }
    }
    if ($data->field_kostenstellen) {
      if (is_string($data->field_kostenstellen)) {
        $data->field_kostenstellen = explode(' | ', $data->field_kostenstellen);
      }
    }
    if ($data->field_funktionen) {
      if (is_string($data->field_funktionen)) {
        $data->field_funktionen = explode(' | ', $data->field_funktionen);
      }
    }
    if ($data->field_benutzer) {
      if (is_string($data->field_benutzer)) {
        $data->field_benutzer = explode(' | ', $data->field_benutzer);
      }
    }
    return $data;
  }

}
