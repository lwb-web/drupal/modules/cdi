<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;

class VDBMusterschreiben extends NodeSchema {

  /**
   * VDBMusterschreiben constructor.
   */
  public function __construct() {
    parent::__construct('musterschreiben', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->fields[] = 'title';
    $this->schema->setGroupBy(TRUE);
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_documents',
        'alias' => 'documents',
        'fields' => [
          'field_documents_fid' => 'field_documents'
        ],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_documents' => 'GROUP_CONCAT(DISTINCT field_documents_fid SEPARATOR \' | \')'],
      ]
    );
    $this->schema->setCount();
    $this->schema->setBeforeInsert([self::class, 'prepareData']);
    return $this->schema;
  }

  /**
   * @param $data
   *
   * @return \stdClass
   */
  public static function prepareData($data) {
    if ($data->field_documents) {
      if (is_string($data->field_documents)) {
        $files = [];
        foreach (explode(' | ', $data->field_documents) as $file) {
          $files[] = [
            'target_id' => $file
          ];
        }
        $data->field_documents = $files;
      }
    }
    return $data;
  }

}
