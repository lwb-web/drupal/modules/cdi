<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;

class VDBVertrag extends NodeSchema {

  /**
   * VDBVertrag constructor.
   */
  public function __construct() {
    parent::__construct('vdb_contract', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->fields[] = 'title';
    $this->schema->setGroupBy(TRUE);
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_vdb_contract_desc',
        'alias' => 'vdb_contract_desc',
        'fields' => ['field_vdb_contract_desc_value' => 'field_vdb_contract_desc'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_abg',
        'alias' => 'vdb_contract_abg',
        'fields' => ['field_vdb_contract_abg_value' => 'field_vdb_contract_abg'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_initiator',
        'alias' => 'vdb_contract_initiator',
        'fields' => ['field_vdb_contract_initiator_value' => 'field_vdb_contract_initiator'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_group',
        'alias' => 'vdb_contract_group',
        'fields' => ['field_vdb_contract_group_value' => 'field_vdb_contract_group'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_partnet',
        'alias' => 'vdb_contract_partnet',
        'fields' => ['field_vdb_contract_partnet_value' => 'field_vdb_contract_partnet'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_date',
        'alias' => 'vdb_contract_date',
        'fields' => ['field_vdb_contract_date_value' => 'field_vdb_contract_date'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_subject',
        'alias' => 'vdb_contract_subject',
        'fields' => ['field_vdb_contract_subject_value' => 'field_vdb_contract_subject'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_contract_runtime_text',
        'alias' => 'contract_runtime_text',
        'fields' => ['field_contract_runtime_text_value' => 'field_contract_runtime_text'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_additional',
        'alias' => 'vdb_contract_additional',
        'fields' => ['field_vdb_contract_additional_value' => 'field_vdb_contract_additional'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_arrangements',
        'alias' => 'vdb_contract_arrangements',
        'fields' => ['field_vdb_contract_arrangements_value' => 'field_vdb_contract_arrangements'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_postscript',
        'alias' => 'vdb_contract_postscript',
        'fields' => ['field_vdb_contract_postscript_value' => 'field_vdb_contract_postscript'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_agreement',
        'alias' => 'vdb_contract_agreement',
        'fields' => ['field_vdb_contract_agreement_value' => 'field_vdb_contract_agreement'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_amount',
        'alias' => 'vdb_contract_amount',
        'fields' => ['field_vdb_contract_amount_value' => 'field_vdb_contract_amount'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_costsperyear',
        'alias' => 'vdb_contract_costsperyear',
        'fields' => ['field_vdb_contract_costsperyear_value' => 'field_vdb_contract_costsperyear'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_tax',
        'alias' => 'vdb_contract_tax',
        'fields' => ['field_vdb_contract_tax_value' => 'field_vdb_contract_tax'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_contracts',
        'alias' => 'vdb_contract_contracts',
        'fields' => ['field_vdb_contract_contracts_nid' => 'field_vdb_contract_contracts'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_vdb_contract_contracts' => 'GROUP_CONCAT(DISTINCT field_vdb_contract_contracts_nid SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_vdb_contract_req',
        'alias' => 'vdb_contract_req',
        'fields' => ['field_vdb_contract_req_value' => 'field_vdb_contract_req'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_vdb_contract_req' => 'GROUP_CONCAT(DISTINCT field_vdb_contract_req_value SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_vdb_contract_req_other',
        'alias' => 'vdb_contract_req_other',
        'fields' => ['field_vdb_contract_req_other_value' => 'field_vdb_contract_req_other'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_con_pre',
        'alias' => 'vdb_contract_con_pre',
        'fields' => ['field_vdb_contract_con_pre_value' => 'field_vdb_contract_con_pre'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_con_res',
        'alias' => 'vdb_contract_con_res',
        'fields' => ['field_vdb_contract_con_res_value' => 'field_vdb_contract_con_res'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_con_son',
        'alias' => 'vdb_contract_con_son',
        'fields' => ['field_vdb_contract_con_son_value' => 'field_vdb_contract_con_son'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_term_procedur',
        'alias' => 'vdb_contract_term_procedur',
        'fields' => ['field_vdb_contract_term_procedur_value' => 'field_vdb_contract_term_procedur'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_extra_time_op',
        'alias' => 'vdb_contract_extra_time_op',
        'fields' => ['field_vdb_contract_extra_time_op_value' => 'field_vdb_contract_extra_time_op'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_consideration',
        'alias' => 'vdb_contract_consideration',
        'fields' => ['field_vdb_contract_consideration_value' => 'field_vdb_contract_consideration'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_warrenty',
        'alias' => 'vdb_contract_warrenty',
        'fields' => ['field_vdb_contract_warrenty_value' => 'field_vdb_contract_warrenty'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_additional_ar',
        'alias' => 'vdb_contract_additional_ar',
        'fields' => ['field_vdb_contract_additional_ar_value' => 'field_vdb_contract_additional_ar'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_limitation',
        'alias' => 'vdb_contract_limitation',
        'fields' => ['field_vdb_contract_limitation_value' => 'field_vdb_contract_limitation'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_agreements',
        'alias' => 'vdb_contract_agreements',
        'fields' => ['field_vdb_contract_agreements_value' => 'field_vdb_contract_agreements'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_advertising',
        'alias' => 'vdb_contract_advertising',
        'fields' => ['field_vdb_contract_advertising_value' => 'field_vdb_contract_advertising'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_clauses',
        'alias' => 'vdb_contract_clauses',
        'fields' => ['field_vdb_contract_clauses_value' => 'field_vdb_contract_clauses'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_other',
        'alias' => 'vdb_contract_other',
        'fields' => ['field_vdb_contract_other_value' => 'field_vdb_contract_other'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_verteiler',
        'alias' => 'vdb_contract_verteiler',
        'fields' => ['field_vdb_contract_verteiler_value' => 'field_vdb_contract_verteiler'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_vdb_contract_verteiler_kop',
        'alias' => 'vdb_contract_verteiler_kop',
        'fields' => ['field_vdb_contract_verteiler_kop_value' => 'field_vdb_contract_verteiler_kop'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
      ],
      [
        'table' => 'field_data_field_documents',
        'alias' => 'documents',
        'fields' => [
          'field_documents_fid' => 'field_documents',
          'field_documents_description',
        ],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_documents' => 'GROUP_CONCAT(DISTINCT CONCAT(field_documents_fid, \'=>\', field_documents_description) SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_organisationseinheiten',
        'alias' => 'organisationseinheiten',
        'fields' => ['field_organisationseinheiten_target_id' => 'field_organisationseinheiten'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_organisationseinheiten' => 'GROUP_CONCAT(DISTINCT field_organisationseinheiten_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_kostenstellen',
        'alias' => 'kostenstellen',
        'fields' => ['field_kostenstellen_target_id' => 'field_kostenstellen'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_kostenstellen' => 'GROUP_CONCAT(DISTINCT field_kostenstellen_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_funktionen',
        'alias' => 'funktionen',
        'fields' => ['field_funktionen_target_id' => 'field_funktionen'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_funktionen' => 'GROUP_CONCAT(DISTINCT field_funktionen_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_benutzer',
        'alias' => 'benutzer',
        'fields' => ['field_benutzer_target_id' => 'field_benutzer'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_benutzer' => 'GROUP_CONCAT(DISTINCT field_benutzer_target_id SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_vdb_contract_runtime',
        'alias' => 'vdb_contract_runtime',
        'fields' => [
          'field_vdb_contract_runtime_value' => 'field_vdb_contract_runtime_value',
          'field_vdb_contract_runtime_value2' => 'field_vdb_contract_runtime_end_value',
        ],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid'
      ]
    );
    $this->schema->setCount();
    $this->schema->setBeforeInsert([self::class, 'prepareData']);
    return $this->schema;
  }

  /**
   * @param $data
   *
   * @return \stdClass
   */
  public static function prepareData($data) {
    if ($data->field_vdb_contract_date) {
      $data->field_vdb_contract_date = date("Y-m-d", strtotime($data->field_vdb_contract_date));
    }
    if ($data->field_vdb_contract_runtime_value) {
      $data->field_vdb_contract_runtime = [
        'value'=> date("Y-m-d", strtotime($data->field_vdb_contract_runtime_value))
      ];
      unset($data->field_vdb_contract_runtime_value);
      if ($data->field_vdb_contract_runtime_end_value) {
        $data->field_vdb_contract_runtime['end_value'] = date("Y-m-d", strtotime($data->field_vdb_contract_runtime_end_value));
        unset($data->field_vdb_contract_runtime_end_value);
      }
    }
    if ($data->field_documents) {
      if (is_string($data->field_documents)) {
        $files = [];
        foreach (explode(' | ', $data->field_documents) as $file) {
          $f = explode('=>', $file);
          $files[] = [
            'target_id' => $f[0],
            'description' => $f[1],
          ];
        }
        $data->field_documents = $files;
      }
    }
    if ($data->field_vdb_contract_req) {
      if (is_string($data->field_vdb_contract_req)) {
        $data->field_vdb_contract_req = explode(' | ', $data->field_vdb_contract_req);
      }
    }
    if ($data->field_vdb_contract_contracts) {
      if (is_string($data->field_vdb_contract_contracts)) {
        $data->field_vdb_contract_contracts = explode(' | ', $data->field_vdb_contract_contracts);
      }
    }
    if ($data->field_organisationseinheiten) {
      if (is_string($data->field_organisationseinheiten)) {
        $data->field_organisationseinheiten = explode(' | ', $data->field_organisationseinheiten);
      }
    }
    if ($data->field_kostenstellen) {
      if (is_string($data->field_kostenstellen)) {
        $data->field_kostenstellen = explode(' | ', $data->field_kostenstellen);
      }
    }
    if ($data->field_funktionen) {
      if (is_string($data->field_funktionen)) {
        $data->field_funktionen = explode(' | ', $data->field_funktionen);
      }
    }
    if ($data->field_benutzer) {
      if (is_string($data->field_benutzer)) {
        $data->field_benutzer = explode(' | ', $data->field_benutzer);
      }
    }
    return $data;
  }

}
