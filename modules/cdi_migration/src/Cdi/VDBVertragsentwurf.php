<?php

namespace Drupal\cdi_migration\Cdi;

use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi_migration\Cdi\Abstracts\NodeSchema;

class VDBVertragsentwurf extends NodeSchema {

  /**
   * VDBVertragsentwurf constructor.
   */
  public function __construct() {
    parent::__construct('vertragsentwurf', 'node');
  }

  public function get_schema(): CdiSchema {
    $this->schema->fields[] = 'title';
    $this->schema->setGroupBy(TRUE);
    $this->schema->setJoinFields(
      [
        'table' => 'field_data_field_documents',
        'alias' => 'documents',
        'fields' => [
          'field_documents_fid' => 'field_documents'
        ],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_documents' => 'GROUP_CONCAT(DISTINCT field_documents_fid SEPARATOR \' | \')'],
      ],
      [
        'table' => 'field_data_field_kategorie',
        'alias' => 'kategorie',
        'fields' => ['field_kategorie_tid' => 'field_kategorie'],
        'tableId' => 'entity_id',
        'joinTable' => 'n',
        'join_id' => 'nid',
        'expression' => ['field_kategorie' => 'GROUP_CONCAT(DISTINCT field_kategorie_tid SEPARATOR \' | \')'],
      ],
    );
    $this->schema->setCount();
    $this->schema->setBeforeInsert([self::class, 'prepareData']);
    return $this->schema;
  }

  /**
   * @param $data
   *
   * @return \stdClass
   */
  public static function prepareData($data) {
    if ($data->field_kategorie) {
      if (is_string($data->field_kategorie)) {
        $data->field_kategorie = explode(' | ', $data->field_kategorie);
      }
    }
    if ($data->field_documents) {
      if (is_string($data->field_documents)) {
        $files = [];
        foreach (explode(' | ', $data->field_documents) as $file) {
          $files[] = [
            'target_id' => $file
          ];
        }
        $data->field_documents = $files;
      }
    }
    return $data;
  }

}
