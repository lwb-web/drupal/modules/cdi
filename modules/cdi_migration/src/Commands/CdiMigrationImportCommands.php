<?php

namespace Drupal\cdi_migration\Commands;

use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\cdi_migration\Commands
 */
class CdiMigrationImportCommands extends DrushCommands {

  /**
   * Drush command that displays the given text.
   *
   * @command cdi:migration:import
   * @aliases cdi-mi
   * @usage cdi:migration:import
   */
  public function import() {
    cdi_migration_start_import();
  }

}
