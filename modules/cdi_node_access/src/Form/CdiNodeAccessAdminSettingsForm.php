<?php

namespace Drupal\cdi_node_access\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CdiNodeAccessAdminSettingsForm.
 *
 * @package Drupal\cdi_node_access\Form
 */
class CdiNodeAccessAdminSettingsForm extends FormBase {

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new ContentAccessAdminSettingsForm.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(PermissionHandlerInterface $permission_handler, ModuleHandlerInterface $module_handler) {
    $this->permissionHandler = $permission_handler;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.permissions'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cdi_node_access_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node_type = NULL) {
    $storage = [
      'node_type' => $node_type,
    ];
    $form_state->setStorage($storage);

    $form['permissions'] = [
      '#type' => 'fieldset',
      '#title' => t('Role based access control settings'),
      '#collapsible' => TRUE,
      '#attributes' => [
        'class' => ['permissions-fieldset'],
      ],
    ];

    $permissions = $this->permissionHandler->getPermissions();
    $permissions_by_provider = [];
    foreach (_cdi_node_access_get_operations($node_type) as $permission_name) {
      $permissions_by_provider[][$permission_name] = $permissions[$permission_name];
    }
    $role_names = [];
    $role_permissions = [];
    $admin_roles = [];
    foreach (Role::loadMultiple() as $role_name => $role) {
      // Retrieve role names for columns.
      $role_names[$role_name] = $role->label();
      // Fetch permissions for the roles.
      $role_permissions[$role_name] = $role->getPermissions();
      $admin_roles[$role_name] = $role->isAdmin();
    }
    // Store $role_names for use when saving the data.
    $form['role_names'] = [
      '#type' => 'value',
      '#value' => $role_names,
    ];

    foreach ($permissions_by_provider as $permissions) {
      foreach (array_reverse($permissions) as $perm => $perm_item) {
        $form['permissions'][$perm] = [
          '#type' => 'fieldgroup',
          '#title' => $perm_item['title'],
          '#collapsible' => FALSE,
          '#prefix' => '<div class="node_access-div">',
          '#suffix' => '</div>',
        ];
        foreach ($role_names as $rid => $name) {
          $form['permissions'][$perm][$rid] = [
            '#title' => $name,
            '#wrapper_attributes' => [
              'class' => ['checkbox'],
            ],
            '#type' => 'checkbox',
            '#default_value' => in_array($perm, $role_permissions[$rid]) ? 1 : 0,
            '#parents' => [$rid, $perm],
          ];
          if ($admin_roles[$rid]) {
            $form['permissions'][$perm][$rid]['#disabled'] = TRUE;
            $form['permissions'][$perm][$rid]['#default_value'] = TRUE;
          }
        }
      }
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save permissions'),
      '#button_type' => 'primary',
    ];
    $form['#attached']['library'][] = 'cdi_node_access/cdi_node_access.permissions';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('role_names') as $role_name => $name) {
      user_role_change_permissions($role_name, (array) $form_state->getValue($role_name));
    }
    $this->messenger()->addStatus($this->t('The changes have been saved.'));
  }

}
