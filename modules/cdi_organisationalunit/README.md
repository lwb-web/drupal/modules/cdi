# Core Data Import - Organisationseinheit (cdi_organisationalunit)

## Features

* Import von Organisationseinheiten

## Berechtigungen

* add cdi_organisationalunit entities: Anlegen von Organisationseinheiten
* administer cdi_organisationalunit entities: Administration von Organisationseinheiten
* delete cdi_organisationalunit entities: Löschen von Organisationseinheiten
* edit cdi_organisationalunit entities: Editieren von Organisationseinheiten
* view published cdi_organisationalunit entities: Anzeigeberechtigung von veröffentlichten Organisationseinheiten
* view unpublished cdi_organisationalunit entities: Anzeigeberechtigung von unveröffentlichten Organisationseinheiten

## Felder

- ID
- Beschreibung
- Langbeschreibung (ID + Beschreibung)
