<?php

namespace Drupal\cdi_organisationalunit;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Core - Organisational Unit entity.
 *
 * @see \Drupal\cdi_organisationalunit\Entity\CdiOrganisationalUnit.
 */
class CdiOrganisationalUnitAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\cdi_organisationalunit\Entity\CdiOrganisationalUnitInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cdi_organisationalunit entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published cdi_organisationalunit entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit cdi_organisationalunit entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete cdi_organisationalunit entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cdi_organisationalunit entities');
  }

}
