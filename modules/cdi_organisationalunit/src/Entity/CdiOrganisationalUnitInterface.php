<?php

namespace Drupal\cdi_organisationalunit\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Core - Organisational Unit entities.
 *
 * @ingroup cdi_organisationalunit
 */
interface CdiOrganisationalUnitInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Core - Organisational Unit title.
   *
   * @return string
   *   Title of the Core - Organisational Unit.
   */
  public function getTitle();

  /**
   * Sets the Core - Organisational Unit title.
   *
   * @param string $value
   *   The Core - Organisational Unit title.
   *
   * @return \Drupal\cdi_organisationalunit\Entity\CdiOrganisationalUnitInterface
   *   The called Core - Organisational Unit entity.
   */
  public function setTitle($value);

  /**
   * Gets the Core - Organisational Unit Identifier.
   *
   * @return string
   *   Identifier of the Core - Organisational Unit.
   */
  public function getIdentifier();

  /**
   * Sets the Core - Organisational Unit Identifier.
   *
   * @param string $value
   *   The Core - Organisational Unit Identifier.
   *
   * @return \Drupal\cdi_organisationalunit\Entity\CdiOrganisationalUnitInterface
   *   The called Core - Organisational Unit entity.
   */
  public function setIdentifier($value);

  /**
   * Gets the Core - Organisational Unit long description.
   *
   * @return string
   *   Long description of the Core - Organisational Unit.
   */
  public function getLongDesc();

  /**
   * Sets the Core - Organisational Unit long description.
   *
   * @param string $value
   *   The Core - Organisational Unit long description.
   *
   * @return \Drupal\cdi_organisationalunit\Entity\CdiOrganisationalUnitInterface
   *   The called Core - Organisational Unit entity.
   */
  public function setLongDesc($value);

  /**
   * Gets the Core - Organisational Unit creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Core - Organisational Unit.
   */
  public function getCreatedTime();

  /**
   * Sets the Core - Organisational Unit creation timestamp.
   *
   * @param int $timestamp
   *   The Core - Organisational Unit creation timestamp.
   *
   * @return \Drupal\cdi_organisationalunit\Entity\CdiOrganisationalUnitInterface
   *   The called Core - Organisational Unit entity.
   */
  public function setCreatedTime($timestamp);

}
