<?php

namespace Drupal\cdi_organisationalunit\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Core - Organisational Unit entities.
 */
class CdiOrganisationalUnitViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
