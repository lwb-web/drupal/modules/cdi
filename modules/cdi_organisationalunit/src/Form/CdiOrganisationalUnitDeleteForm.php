<?php

namespace Drupal\cdi_organisationalunit\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Core - Organisational Unit entities.
 *
 * @ingroup cdi_organisationalunit
 */
class CdiOrganisationalUnitDeleteForm extends ContentEntityDeleteForm {


}
