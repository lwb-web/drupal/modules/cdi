# Core Data Import - Benutzer (cdi_user)



## Features

* Import von Benutzern
* Rollenvergabe nach Kostenstelle, Organisationseinheit oder Funktion: `Achtung!` Rollen, die automatisch vergeben werden, können nicht per Direktzuweisung verwendet werden
* Anpassen der Darstellung des Namens: Vor- und Nachname oder Nachname, Vorname
* View als Lookup für Entityreferenzen auf den Nutzer

## Felder

- Kontakt
	- Telefon
	- Fax
	- Email
- Internes
	- Login
	- Funktion (cdi_function)
	- Kostenstelle (cdi_costcenter)
	- Organisationseinheit (cdi_organisationalunit)
	- Raum
	- Status (aktiv/inaktiv)
	- Zugehörigkeit (intern/extern) externe Benutzer werden nicht automatisch aktualisiert

## Voraussetzungen

* cdi_function
* cdi_costcenter
* cdi_organisationalunit
