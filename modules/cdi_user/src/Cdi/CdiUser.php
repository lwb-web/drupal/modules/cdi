<?php

namespace Drupal\cdi_user\Cdi;

use Cocur\Slugify\Slugify;
use Drupal\cdi\Cdi\CdiSchema;
use Drupal\cdi\Cdi\CdiTools;
use Drupal\cdi\Cdi\Messenger;
use Drupal\user\Entity\User;

/**
 * Class CdiUser.
 *
 * @package Drupal\cdi_user\Cdi
 */
class CdiUser {

  /**
   * Defines a Schema for Import.
   *
   * @return \Drupal\cdi\Cdi\CdiSchema
   *   returns the Schema.
   */
  public static function schema() {
    $schema = new CdiSchema('core', 'employees', 'user');
    $schema->setAlias('e');
    $schema->setToType('user');
    $schema->setTable('employees');
    $schema->setFields(
      [
        'surname',
        'name',
        'fax',
        'is_active',
        'department_id',
        'function_id',
        'costcenter_id',
        'phone1',
        'phone2',
        'handy',
        'login',
        'email',
        'roomnumber',
      ]
    );
    $schema->setOrderField('e.login');
    $schema->setConditions(
      [
        'e.login',
        '',
        '<>',
      ]
    );
    $schema->setCount();
    $schema->setBeforeInsert([self::class, 'prepareData']);
    $schema->setAfterBatch(
      [self::class, 'assignRoles'], [self::class, 'cleanup']
    );
    return $schema;
  }

  /**
   * Preparing data befor Import.
   *
   * @param object $data
   *   Data for preparing.
   *
   * @return object
   *   return preparing data.
   */
  public static function prepareData($data) {
    $config = \Drupal::config('cdi.settings.user_name');
    $slugify = new Slugify();
    $email = (isset($data->email) && $data->email != '') ?
      $data->email : trim(
        $slugify->slugify($data->name) . '.' . $slugify->slugify($data->surname) . '@lwb.de'
      );
    $entity = new \stdClass();
    $entity->id = $data->login;
    $entity->name = $data->login;
    $entity->mail = $email;
    $entity->field_email = ($data->function_id !== '045') ? $email : '';
    $entity->field_login = $data->login;
    $entity->field_room = $data->roomnumber;
    $entity->field_membership = 0;
    if ($config->get('cdi_user_first_surname')) {
      $entity->field_name = trim($data->surname . ', ' . $data->name);
    }
    else {
      $entity->field_name = trim($data->name . ' ' . $data->surname);
    }
    $entity->field_is_active = $data->is_active;
    $entity->status          = $data->is_active;
    if ($data->costcenter_id) {
      $entity->field_costcenter = CdiTools::loadEntityByField(
        'cdi_costcenter', 'core_identifier',
        CdiTools::trimNumeric($data->costcenter_id)
      );
      if (!$entity->field_costcenter) {
        $values = [
          '%user'       => $data->login,
          '%costcenter' => $data->costcenter_id,
        ];

        \Drupal::logger('cdi_user')->error(
          t('user: %user costcenter: %costcenter', $values)
        );
        Messenger::sendError(
          'Costcenter not found',
          'user: ' . $data->login . ' costcenter: ' . $data->costcenter_id,
          'cdi_user', 'cdi_user_import'
        );
      }
    }
    if ($data->department_id) {
      $entity->field_organisationalunit = CdiTools::loadEntityByField(
        'cdi_organisationalunit', 'core_identifier',
        CdiTools::trimNumeric($data->department_id)
      );
      if (!$entity->field_organisationalunit) {
        $values = [
          '%user'        => $data->login,
          '%departement' => $data->department_id,
        ];

        \Drupal::logger('cdi_user')->error(
          t('user: %user departement: %departement', $values)
        );
        Messenger::sendError(
          'Department/Organisational unit not found',
          'user: ' . $data->login . ' departement: ' . $data->department_id,
          'cdi_user', 'cdi_user_import'
        );
      }
    }
    if ($data->function_id) {
      $entity->field_function = CdiTools::loadEntityByField(
        'cdi_function', 'core_identifier', $data->function_id
      );
      if (!$entity->field_function) {
        $values = [
          '%user'     => $data->login,
          '%function' => $data->function_id,
        ];
        \Drupal::logger('cdi_user')->error(
          t('user: %user function: %function', $values)
        );
        Messenger::sendError(
          'Function not found',
          'user: ' . $data->login . ' function: ' . $data->function_id,
          'cdi_user', 'cdi_user_import'
        );
      }
    }
    if ($data->phone1) {
      $entity->field_phone[] = $data->phone1;
    }
    if ($data->phone2) {
      $entity->field_phone[] = $data->phone2;
    }
    if ($data->handy) {
      $entity->field_phone[] = $data->handy;
    }
    return $entity;
  }

  /**
   * Assign roles after imports.
   *
   * @param array|object $context
   *   Batch context for messages.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function assignRoles(&$context) {
    $config = \Drupal::config('cdi.settings.user_roles');
    $assignments = $config->get('cdi_user_assignments');

    if (empty($assignments)) {
      return;
    }

    $context['message'] = t('Remove User Roles');

    // Cleanup roles.
    foreach ($assignments as $key => $value) {
      $values = explode("|", $key);
      self::removeAssignment($values[0]);
    }

    foreach ($assignments as $key => $value) {
      $values = explode("|", $key);
      $rid = $values[0];
      $costcenter_id = $values[1];
      $organisationalunit_id = $values[2];
      $function_id = $values[3];
      $properties = [];
      $context['message'] = t('Assign role by rule:
        [role]: @role||
        [costcenter]: @costcenter ||
        [organisationalunit]: @organisationalunit ||
        [function]: @function', [
          '@role' => $rid,
          '@costcenter' => $costcenter_id,
          '@organisationalunit' => $organisationalunit_id,
          '@function' => $function_id,
        ]);
      if ($costcenter_id > 0) {
        $properties['field_costcenter'] = $costcenter_id;
      }

      if ($organisationalunit_id > 0) {
        $properties['field_organisationalunit'] = $organisationalunit_id;
      }

      if ($function_id > 0) {
        $properties['field_function'] = $function_id;
      }

      if (!empty($properties)) {
        $properties['status'] = 1;

        $users = \Drupal::entityTypeManager()
          ->getStorage('user')
          ->loadByProperties($properties);

        foreach ($users as $user) {
          $user->addRole($rid);
          $user->save();
        }
      }
    }
  }

  /**
   * Cleanup after Import.
   *
   * @param array|object $context
   *   Batch context for messages.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function cleanup(&$context) {
    $context['message'] = t('Clean up users');
    $users = CdiTools::loadEntitiesByField(
      'user', ['field_membership' => 0]
    );
    foreach ($users as $user) {
      $core_user = new CdiSchema('core', 'employees', 'user');
      $core_user->setTable('employees');
      $core_user->setAlias('e');
      $core_user->setFields(['login']);
      $core_user->setOrderField('e.login');
      $core_user->setConditions(['e.login', $user->getAccountName()]);
      $core_user->setCount();
      if ($core_user->count == 0) {
        $user->set('status', 0);
        $user->save();
      }
    }

    $database = \Drupal::service('database');
    $result = $database->query("INSERT INTO {authmap} 
      SELECT uid, 'cas' AS provider, `name` AS authna, :data  
      FROM {users_field_data}
      WHERE `name` NOT IN (SELECT authname FROM {authmap} WHERE provider = 'cas')
      AND `status` = 1", [':data' => 'N;']);    
  }

  /**
   * Removes the assignment to role with rid.
   *
   * @param int $rid
   *   ID of role.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private static function removeAssignment($rid) {
    $ids = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('roles', $rid)
      ->execute();
    $users = User::loadMultiple($ids);
    foreach ($users as $user) {
      $user->removeRole($rid);
      $user->save();
    }
  }

}
