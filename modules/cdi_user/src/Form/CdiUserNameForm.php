<?php

namespace Drupal\cdi_user\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CdiUserRolesForm.
 */
class CdiUserNameForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cdi.settings.user_name',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cdi_settings_user_name_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cdi.settings.user_name');
    $form = parent::buildForm($form, $form_state);

    $form['cdi_user_first_surname'] = [
      '#type' => 'radios',
      '#title' => t('Nachname als erstes?'),
      '#options' => [
        t('No'),
        t('Yes'),
      ],
      '#default_value' => $config->get('cdi_user_first_surname'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('cdi.settings.user_name')
      ->set('cdi_user_first_surname', $values['cdi_user_first_surname'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
