<?php

namespace Drupal\cdi_user\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class CdiUserRolesDeleteForm.
 */
class CdiUserRolesDeleteForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cdi.settings.user_roles',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cdi_settings_user_roles_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $key = NULL) {
    $config = $this->config('cdi.settings.user_roles');
    $assignments = $config->get('cdi_user_assignments') ?? [];
    if (!$key || !$assignments[$key]) {
      return $this->redirect('cdi.settings.user_roles');
    }

    $form = parent::buildForm($form, $form_state);
    $form['agree'] = [
      '#markup' => $this->t("Delete the entry?"),
      '#weight' => -99,
    ];
    $form['key'] = [
      '#type' => 'hidden',
      '#value' => $key,
    ];

    $form['actions']['submit']['#value'] = $this->t('Delete');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cdi.settings.user_roles');
    $values = $form_state->getValues();
    $assignments = $config->get('cdi_user_assignments');
    if ($values['key'] && $assignments[$values['key']]) {
      unset($assignments[$values['key']]);
      $config
        ->set('cdi_user_assignments', $assignments)
        ->save();
    }
    parent::submitForm($form, $form_state);
    $form_state->setRedirectUrl(Url::fromRoute('cdi.settings.user_roles'));
  }

}
