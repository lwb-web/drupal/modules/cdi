<?php

namespace Drupal\cdi_user\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class CdiUserRolesForm.
 */
class CdiUserRolesForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cdi.settings.user_roles',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cdi_settings_user_roles_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cdi.settings.user_roles');
    $assignments = $config->get('cdi_user_assignments') ?? [];
    $roles = [''];
    foreach (user_roles(TRUE) as $role) {
      $roles[$role->id()] = $role->label();
    }
    asort($roles);

    // Build table.
    $form['roles'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Rolle'),
        $this->t('Kostenstelle'),
        $this->t('Organisationseinheit'),
        $this->t('Funktion'),
        $this->t('Operationen'),
      ],
      '#empty' => $this->t('No rules.'),
      '#tableselect' => FALSE,
      '#tree' => TRUE,
    ];

    $costcenters = $this->getEntities('cdi_costcenter');
    $organisationalunits = $this->getEntities('cdi_organisationalunit');
    $functions = $this->getEntities('cdi_function');
    $order = 0;

    foreach ($assignments as $key => $value) {
      $values = explode("|", $key);

      $form['roles'][$order]['#role'] = $key;

      $form['roles'][$order]['role'] = [
        '#markup' => $roles[$values[0]],
      ];

      $form['roles'][$order]['costcenter'] = [
        '#markup' => $costcenters[$values[1]],
      ];

      $form['roles'][$order]['organisationalunit'] = [
        '#markup' => $organisationalunits[$values[2]],
      ];

      $form['roles'][$order]['function'] = [
        '#markup' => $functions[$values[3]],
      ];

      $form['roles'][$order]['operation'] = [
        '#type' => 'link',
        '#title' => [
          '#markup' => t('Delete'),
        ],
        '#url' => Url::fromRoute('cdi.settings.user_roles.delete', ['key' => $key]),
      ];

      $order++;
    }

    $form['roles'][-1]['#role'] = -1;
    $form['roles'][-1]['role'] = [
      '#type' => 'select',
      '#title' => 'role',
      '#title_display' => 'invisible',
      '#options' => $roles,
    ];

    $form['roles'][-1]['costcenter'] = [
      '#type' => 'select',
      '#title' => 'Kostenstelle',
      '#title_display' => 'invisible',
      '#options' => $costcenters,
    ];

    $form['roles'][-1]['organisationalunit'] = [
      '#type' => 'select',
      '#title' => 'Organisationseinheiten',
      '#title_display' => 'invisible',
      '#options' => $organisationalunits,
    ];

    $form['roles'][-1]['function'] = [
      '#type' => 'select',
      '#title' => 'Funktionen',
      '#title_display' => 'invisible',
      '#options' => $functions,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cdi.settings.user_roles');
    $values = $form_state->getValues()['roles'][-1];

    $role = $values['role'];
    $costcenter = $values['costcenter'];
    $organisationalunit = $values['organisationalunit'];
    $function = $values['function'];
    $assignments = $config->get('cdi_user_assignments') ?? [];
    if (!empty($role) && ($costcenter > 0 || $organisationalunit > 0 || $function > 0)) {
      $key = implode("|", [
        $role,
        $costcenter,
        $organisationalunit,
        $function,
      ]);
      $value = [
        $key => 'TRUE',
      ];
      if (!array_key_exists($key, $assignments)) {
        $config
          ->set('cdi_user_assignments', array_merge($assignments, $value))
          ->save();
      }

    }
    parent::submitForm($form, $form_state);
  }

  /**
   * Get multiple Entities.
   *
   * @param string $type
   *   The entity type.
   *
   * @return string[]
   *   Return a list of entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getEntities(string $type) {
    $list = [''];
    $results = \Drupal::entityTypeManager()->getStorage($type)
      ->loadByProperties(['status' => 1]);
    foreach ($results as $data) {
      $list[$data->id()] = $data->label();
    }
    asort($list);
    return $list;
  }

}
