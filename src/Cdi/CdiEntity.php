<?php

namespace Drupal\cdi\Cdi;

use Drupal\cdi_companycode\Entity\CdiCompanyCode;
use Drupal\cdi_costcenter\Entity\CdiCostCenter;
use Drupal\cdi_district\Entity\CdiDistrict;
use Drupal\cdi_function\Entity\CdiFunction;
use Drupal\cdi_organisationalunit\Entity\CdiOrganisationalUnit;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Class to Create or Update Drupal Entities.
 *
 * @package Drupal\cdi\Cdi
 */
class CdiEntity {

  /**
   * Create or update entities for specific types.
   *
   * @param string $type
   *   The entity type to create or update.
   * @param object $data
   *   Object with the entity data.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Returns Entity or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function insert(string $type, object $data) {
    $id = $data->id ?? -1;
    switch ($type) {
      case 'term':
        $entity = Term::load($id) ?? Term::create(['vid' => $data->vid]);
        break;

      case 'file':
        $entity = File::load($id) ?? File::create();
        break;

      case 'user':
        $entity = CdiTools::loadEntityByField('user', 'field_login', $data->field_login) ??
        User::load($id) ??
        User::create();
        break;

      case 'rdb_instance':
        $entity = Paragraph::load($id) ??
                  Paragraph::create(['type' => 'rdb_instance', 'id' => $id]);
        break;

      case 'vdb_limit':
        $entity = Paragraph::load($id) ??
                  Paragraph::create(['type' => 'vdb_limit', 'id' => $id]);
        break;

      case 'node':
        $entity = Node::load($id) ?? Node::create(['type' => $data->type]);
        break;

      case 'costcenter':
        if (isset($data->core_identifier)) {
          $data->core_identifier = CdiTools::trimNumeric($data->core_identifier);
        }
        $entity =
          CdiTools::loadEntityByField('cdi_costcenter', 'core_identifier', $data->core_identifier) ??
          CdiCostCenter::load($id) ??
          CdiCostCenter::create();
        break;

      case 'companycode':
        if (isset($data->core_identifier)) {
          $data->core_identifier = CdiTools::trimNumeric($data->core_identifier);
        }
        $entity =
          CdiTools::loadEntityByField('cdi_companycode', 'core_identifier', $data->core_identifier) ??
          CdiCompanyCode::load($id) ??
          CdiCompanyCode::create();
        break;

      case 'district':
        if (isset($data->core_identifier)) {
          $data->core_identifier = CdiTools::trimNumeric($data->core_identifier);
        }
        $entity =
          CdiTools::loadEntityByField('cdi_district', 'core_identifier', $data->core_identifier) ??
          CdiDistrict::load($id) ??
          CdiDistrict::create();
        break;

      case 'organisationalunit':
        if (isset($data->core_identifier)) {
          $data->core_identifier = CdiTools::trimNumeric($data->core_identifier);
        }
        $entity =
          CdiTools::loadEntityByField('cdi_organisationalunit', 'core_identifier', $data->core_identifier) ??
          CdiOrganisationalUnit::load($id) ??
          CdiOrganisationalUnit::create();
        break;

      case 'function':
        $entity =
          CdiTools::loadEntityByField('cdi_function', 'core_identifier', $data->core_identifier) ??
          CdiFunction::load($id) ??
          CdiFunction::create();
        break;

      default:
        $entity = NULL;
    }
    if ($entity) {
      foreach ($data as $key => $value) {
        try {
          $entity->set($key, $value);
        }
        catch (\InvalidArgumentException $e) {

        }
      }
      try {
        $entity->save();
        return $entity;
      }
      catch (EntityStorageException $e) {
        \Drupal::logger('cdi_insert')->error('Error on insert @type: @error', [
          '@type' => $type,
          '@error' => $e->getMessage(),
        ]);
      }
    }
    return FALSE;
  }

}
