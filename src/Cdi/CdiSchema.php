<?php

namespace Drupal\cdi\Cdi;

use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class to build a database select query, get this results and counts.
 *
 * @package Drupal\cdi\Cdi
 */
class CdiSchema {

  /**
   * Count total.
   *
   * @var int
   */
  public int $count = 0;

  /**
   * Batch limit.
   *
   * @var int
   */
  public int $limit = 100;

  /**
   * Functions to run before insert.
   *
   * @var array|null
   */
  public ?array $beforeInsert = NULL;

  /**
   * Functions to run after insert.
   *
   * @var array|null
   */
  public ?array $afterInsert = NULL;

  /**
   * Functions to run before batch processing.
   *
   * @var array|null
   */
  public ?array $beforeBatch = NULL;

  /**
   * Functions to run after batch processing.
   *
   * @var array|null
   */
  public ?array $afterBatch = NULL;

  /**
   * Database key to select a database.
   *
   * @var string
   */
  public string $databaseKey;

  /**
   * Schema name.
   *
   * @var string
   */
  public string $name;

  /**
   * Schema type.
   *
   * @var string
   */
  public string $type;

  /**
   * Schema to type (node to entity).
   *
   * @var string
   */
  public string $toType;

  /**
   * Schema table.
   *
   * @var string
   */
  public string $table;

  /**
   * Schema alias.
   *
   * @var string|null
   */
  public ?string $alias = NULL;

  /**
   * Schema fields.
   *
   * @var string[]|null
   */
  public ?array $fields = NULL;

  /**
   * Schema field to sort.
   *
   * @var string
   */
  public string $orderField;

  /**
   * Schema sort direction.
   *
   * @var string
   */
  public string $orderDirection = 'ASC';

  /**
   * Schema conditions.
   *
   * @var array
   */
  public array $conditions = [];

  /**
   * Schema expressions.
   *
   * @var array|null
   */
  public ?array $expression = NULL;

  /**
   * Schema where expression.
   *
   * @var array
   */
  public array $whereExpression = [];

  /**
   * Schema grouping.
   *
   * @var bool
   */
  public bool $groupBy = FALSE;

  /**
   * Schema joins.
   *
   * @var \Drupal\cdi\Cdi\CdiSchemaField[]
   */
  public array $joinFields = [];

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection|null
   */
  private $database;

  /**
   * CdiSchema constructor.
   *
   * @param string $databaseKey
   *   Database key to select a database.
   * @param string $name
   *   Schema name.
   * @param string $type
   *   Schema type.
   */
  public function __construct(string $databaseKey, string $name, string $type) {
    $this->setDatabaseKey($databaseKey);
    $this->setName($name);
    $this->setType($type);
    $this->setToType($type);
  }

  /**
   * Set Database key.
   *
   * @param string $databaseKey
   *   Database key to select a database.
   */
  public function setDatabaseKey(string $databaseKey): void {
    $this->databaseKey = $databaseKey;
  }

  /**
   * Set Name.
   *
   * @param string $name
   *   Schema name.
   */
  public function setName(string $name): void {
    $this->name = $name;
  }

  /**
   * Set before insert.
   *
   * @param array|null $beforeInsert
   *   Functions to run before insert.
   */
  public function setBeforeInsert(?array $beforeInsert): void {
    $this->beforeInsert = $beforeInsert;
  }

  /**
   * Set after insert.
   *
   * @param array|null $afterInsert
   *   Functions to run after insert.
   */
  public function setAfterInsert(?array $afterInsert): void {
    $this->afterInsert = $afterInsert;
  }

  /**
   * Set before batch.
   *
   * @param array|null $beforeBatch
   *   Functions to run before batch processing.
   */
  public function setBeforeBatch(...$beforeBatch): void {
    $this->beforeBatch = $beforeBatch;
  }

  /**
   * Set after batch.
   *
   * @param array|null $afterBatch
   *   Functions to run after batch processing.
   */
  public function setAfterBatch(...$afterBatch): void {
    $this->afterBatch = $afterBatch;
  }

  /**
   * Set group by.
   *
   * @param bool $groupBy
   *   Schema grouping.
   */
  public function setGroupBy(bool $groupBy): void {
    $this->groupBy = $groupBy;
  }

  /**
   * Set type.
   *
   * @param string $type
   *   Schema type.
   */
  public function setType(string $type): void {
    $this->type = $type;
  }

  /**
   * Set to type.
   *
   * @param string $toType
   *   Schema to type.
   */
  public function setToType(string $toType): void {
    $this->toType = $toType;
  }

  /**
   * Set table.
   *
   * @param string $table
   *   Schema table.
   */
  public function setTable(string $table): void {
    $this->table = $table;
    if (!$this->alias) {
      $this->setAlias($table);
    }
  }

  /**
   * Set alias.
   *
   * @param string $alias
   *   Schema alias.
   */
  public function setAlias(string $alias): void {
    $this->alias = $alias;
  }

  /**
   * Set fields.
   *
   * @param string[] $fields
   *   Schema fields.
   */
  public function setFields(array $fields): void {
    $this->fields = $fields;
  }

  /**
   * Set order field.
   *
   * @param string $orderField
   *   Schema order field.
   */
  public function setOrderField(string $orderField): void {
    $this->orderField = $orderField;
  }

  /**
   * Set order direction.
   *
   * @param string $orderDirection
   *   Schema order direction.
   */
  public function setOrderDirection(string $orderDirection): void {
    $this->orderDirection = $orderDirection;
  }

  /**
   * Set conditions.
   *
   * @param array $conditions
   *   Schema conditions.
   */
  public function setConditions(array ...$conditions): void {
    $this->conditions = $conditions;
  }

  /**
   * Set expressions.
   *
   * @param array $expression
   *   Schema expressions.
   */
  public function setExpression(array $expression): void {
    $this->expression = $expression;
  }

  /**
   * Set where expressions.
   *
   * @param array $whereExpression
   *   Schema where expressions.
   */
  public function setWhereExpression(array ...$whereExpression): void {
    $this->whereExpression = $whereExpression;
  }

  /**
   * Set join fields.
   *
   * @param array $joinFields
   *   Schema join fields.
   */
  public function setJoinFields(array ...$joinFields): void {
    $f = [];
    foreach ($joinFields as $joinField) {
      $f[] = new CdiSchemaField($joinField);
    }
    $this->joinFields = $f;
  }

  /**
   * Set Count of found database rows.
   */
  public function setCount() {
    $this->setDatabase();
    $query = $this->createQuery();
    $this->count = $query->countQuery()->distinct()->execute()->fetchField();
    $this->resetDatabase();
  }

  /**
   * Debug function.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   Return query.
   */
  public function showQuery() {
    $this->setDatabase();
    $query = $this->createQuery();
    $this->resetDatabase();
    return $query->execute();
  }

  /**
   * Get database results.
   *
   * @param int|null $offset
   *   Offset for the query.
   * @param int|null $limit
   *   Limit for the query.
   *
   * @return mixed
   *   An array of results.
   */
  public function getDatabaseData($offset = NULL, $limit = NULL) {
    $this->setDatabase();
    $query = $this->createQuery();
    if ($offset !== NULL && $limit !== NULL) {
      $query->range($offset, $limit);
    }
    $result = $query->distinct()->execute()->fetchAll();
    $this->resetDatabase();
    return $result;
  }

  /**
   * Call function before Insert.
   *
   * @param callable $callback
   *   Callback function.
   * @param object $entity
   *   To insert entity.
   *
   * @return object
   *   Changed or edited entity.
   */
  private function callBeforeInsert(callable $callback, object $entity) {
    return $callback($entity);
  }

  /**
   * Call function after Insert.
   *
   * @param callable $callback
   *   Callback function.
   * @param object $entity
   *   To insert entity.
   * @param \Drupal\Core\Entity\EntityInterface $insertedEntity
   *   To inserted entity.
   */
  private function callAfterInsert(callable $callback, object $entity, EntityInterface $insertedEntity) {
    $callback($entity, $insertedEntity);
  }

  /**
   * Import data results.
   *
   * @param array $data
   *   Query results as array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function importData(array $data) {
    foreach ($data as $entity) {
      if ($this->beforeInsert) {
        $entity = $this->callBeforeInsert($this->beforeInsert, $entity);
      }
      $insertedEntity = CdiEntity::insert($this->toType, $entity);
      if ($this->afterInsert) {
        $this->callAfterInsert($this->afterInsert, $entity, $insertedEntity);
      }
    }
  }

  /**
   * Create a Database Query based on Class properties.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   returns created query.
   */
  private function createQuery() {
    $query = $this->database->select($this->table, $this->alias);
    if ($this->fields) {
      foreach ($this->fields as $key => $value) {
        $query->addField($this->alias, (!is_numeric($key)) ? $key : $value, $value);
        if ($this->groupBy) {
          $query->groupBy((!is_numeric($key)) ? $key : $value);
        }
      }
    }
    else {
      $query->fields($this->alias);
    }
    if ($this->joinFields) {
      foreach ($this->joinFields as $joinField) {
        $joinField->addQuery($query, $this->groupBy);
      }
    }
    if ($this->expression) {
      foreach ($this->expression as $key => $value) {
        $query->addExpression($value, $key);
      }
    }
    foreach ($this->conditions as $condition) {
      $query->condition($condition[0], $condition[1], (isset($condition[2])) ? $condition[2] : '=');
    }
    foreach ($this->whereExpression as $where) {
      if ((isset($where[1]))) {
        $query->where($where[0], $where[1]);
      }
      else {
        $query->where($where[0]);
      }
    }
    $query->orderBy($this->orderField, $this->orderDirection);
    return $query;
  }

  /**
   * Connect to another Database connection.
   */
  private function setDatabase() {
    Database::setActiveConnection($this->databaseKey);
    $this->database = Database::getConnection();
  }

  /**
   * Reset the Database connection to default (D8).
   */
  private function resetDatabase() {
    Database::setActiveConnection();
    $this->database = NULL;
  }

}
