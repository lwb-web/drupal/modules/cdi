<?php

namespace Drupal\cdi\Cdi;

use Drupal\Core\Database\Query\Select;

/**
 * Class to build SQL joins for CdiSchema Select Querys.
 *
 * @package Drupal\cdi\Cdi
 */
class CdiSchemaField {

  /**
   * Schema Type.
   *
   * @var string
   */
  public string $type;

  /**
   * Schema Table.
   *
   * @var string
   */
  public string $table;

  /**
   * Table alias.
   *
   * @var string
   */
  public string $alias;

  /**
   * Table fields.
   *
   * @var array
   */
  public array $fields;

  /**
   * Table id.
   *
   * @var string
   */
  public string $tableId;

  /**
   * Table to Join.
   *
   * @var string
   */
  public string $joinTable;

  /**
   * Join id.
   *
   * @var string
   */
  public string $joinId;

  /**
   * Join type.
   *
   * @var string
   */
  public string $joinType = 'LeftJoin';

  /**
   * SQL expressions.
   *
   * @var array|null
   */
  public ?array $expression = NULL;

  /**
   * CdiSchemaField constructor.
   *
   * @param array $joinField
   *   Fields of joined tables.
   */
  public function __construct(array $joinField) {
    $this->table = $joinField['table'];
    $this->fields = $joinField['fields'];
    if (isset($joinField['alias'])) {
      $this->alias = $joinField['alias'];
    }
    else {
      $this->alias = $joinField['table'];
    }
    $this->tableId = $joinField['tableId'];
    $this->joinTable = $joinField['joinTable'];
    $this->joinId = $joinField['join_id'];
    if (isset($joinField['joinType'])) {
      $this->joinType = $joinField['joinType'];
    }
    if (isset($joinField['expression'])) {
      $this->expression = $joinField['expression'];
    }
  }

  /**
   * Add query for joining fields.
   *
   * @param \Drupal\Core\Database\Query\Select $query
   *   Current query.
   * @param bool $groupBy
   *   Grouping fields.
   */
  public function addQuery(Select $query, $groupBy = FALSE) {
    switch ($this->joinType) {
      case 'InnerJoin':
        $query->innerJoin($this->table, $this->alias, "$this->joinTable.$this->joinId = $this->alias.$this->tableId");
        break;

      case 'RightJoin':
        $query->rightJoin($this->table, $this->alias, "$this->joinTable.$this->joinId = $this->alias.$this->tableId");
        break;

      default:
        $query->leftJoin($this->table, $this->alias, "$this->joinTable.$this->joinId = $this->alias.$this->tableId");
        break;
    }
    if ($this->expression) {
      foreach ($this->expression as $key => $value) {
        $query->addExpression($value, $key);
      }
    }
    else {
      foreach ($this->fields as $key => $value) {
        $query->addField($this->alias, (!is_numeric($key)) ? $key : $value, $value);
        if ($groupBy) {
          $query->groupBy((!is_numeric($key)) ? $key : $value);
        }
      }
    }
  }

}
