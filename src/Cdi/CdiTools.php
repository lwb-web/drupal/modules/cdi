<?php

namespace Drupal\cdi\Cdi;

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ExtensionPathResolver;

/**
 * Class CdiTools.
 *
 * @package Drupal\cdi\Cdi
 */
class CdiTools {

  /**
   * Trims a value.
   *
   * @param string $value
   *   Value to trim.
   *
   * @return string
   *   Trimmed value.
   */
  public static function trimNumeric($value) {
    return (is_numeric($value)) ? intval($value) : $value;
  }

  /**
   * Install and update helper for config files from custom modules.
   *
   * @param string $module
   *   The module name.
   * @param string $config_name
   *   The config file name without .yml.
   * @param string $config_path
   *   The relative path to the folder of the configuration file within the
   *   module folder. Default '/config/optional'.
   */
  public static function importSingleConfigFromModule($module, $config_name, $config_path = '/config/optional') {
    $config_path = ExtensionPathResolver::getPath('module', $module) . $config_path;
    $source = new FileStorage($config_path);
    $config_storage = \Drupal::service('config.storage');
    $config_storage->write($config_name, $source->read($config_name));
  }

  /**
   * Delete helper for config files from custom modules.
   *
   * @param string $config_name
   *   The config file name without .yml.
   */
  public static function deleteSingleConfigFromModule($config_name) {
    $config_storage = \Drupal::service('config.storage');
    $config_storage->delete($config_name);
  }

  /**
   * Method to check Entity has field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to check.
   * @param string $field_name
   *   The field name.
   *
   * @return bool
   *   Returns true or false.
   */
  public static function entityHasField(EntityInterface $entity, string $field_name): bool {
    $exists = FALSE;

    if (!is_null($entity)) {
      if (isset($entity->{$field_name}) && $entity->hasField($field_name)) {
        $exists = TRUE;
      }
    }

    return $exists;
  }

  /**
   * Method to load Entity by id.
   *
   * @param string $entity_type
   *   Type of the Entity to load.
   * @param string|int $id
   *   Id of the Entity to load.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Returns loaded Entity or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function loadEntityById(string $entity_type, $id): ?EntityInterface {
    return \Drupal::entityTypeManager()
      ->getStorage($entity_type)
      ->load($id);
  }

  /**
   * Method to load Entity by field value.
   *
   * @param string $entity_type
   *   Type of the Entity to load.
   * @param string $field
   *   Field of the Entity to load.
   * @param string $value
   *   Value of the field.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Returns loaded Entity or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function loadEntityByField(string $entity_type, string $field, string $value): ?EntityInterface {
    $items = \Drupal::entityTypeManager()
      ->getStorage($entity_type)
      ->loadByProperties([$field => $value]);
    return (count($items) > 0) ? $items[array_key_first($items)] : NULL;
  }

  /**
   * Method to load multiple Entities by multiple field values.
   *
   * @param string $entity_type
   *   Type of the Entity to load.
   * @param array $filters
   *   Array of filters, like field_name as key, field_value as value.
   * @param string $bundle
   *   Value of the field.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Returns array of Entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function loadEntitiesByField(string $entity_type, array $filters = [], $bundle = NULL): array {
    if ($bundle) {
      $filters['type'] = $bundle;
    }
    return \Drupal::entityTypeManager()
      ->getStorage($entity_type)
      ->loadByProperties($filters);
  }

  /**
   * Load a user Entity by specific permission.
   *
   * @param string $permission
   *   Permission of the user.
   * @param bool $excludeAdmin
   *   Exclude admin role true|false.
   * @param bool $excludeGuest
   *   Exclude guest role true|false.
   *
   * @return array
   *   Return array of Users.
   */
  public static function loadUsersByPermission(string $permission, $excludeAdmin = FALSE, $excludeGuest = TRUE): array {
    $roles = user_role_names($excludeGuest, $permission);
    if ($excludeAdmin) {
      unset($roles['administrator']);
    }
    $result = \Drupal::service('entity_type.manager')
      ->getStorage('user')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('roles', array_keys($roles), 'IN')
      ->execute();
    return array_keys($result);
  }

}
