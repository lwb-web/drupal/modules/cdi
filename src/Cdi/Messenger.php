<?php

namespace Drupal\cdi\Cdi;

use Drupal\user\Entity\User;

/**
 * Class to send messages.
 */
class Messenger {

  /**
   * Constant to show it'S an error message.
   *
   * @var int
   */
  const ERROR = 1;

  /**
   * Constant to show it's a debug message.
   *
   * @var int
   */
  const DEBUG = 2;

  /**
   * Constant to show it's an event message.
   *
   * @var int
   */
  const EVENT = 3;

  /**
   * IP of message server.
   *
   * @var string
   */
  const IP = '10.100.133.33';

  /**
   * Port of IP server.
   *
   * @var number
   */
  const PORT = 10065;

  /**
   * Function to send the message.
   *
   * @param int $type
   *   Type of message.
   * @param string $message
   *   Messagetext of message.
   * @param string $stack
   *   Stack of message.
   * @param string $class
   *   Class of message.
   * @param string $function
   *   Function of message.
   */
  public static function send($type, $message, $stack, $class, $function) {
    global $base_url;
    $user = \Drupal::currentUser();

    $user_name = ($user->isAuthenticated()) ? $user->getAccountName() : 'cron';
    $text = '|||t|||' . $type . '|||d|||' . $message . '|||s|||' . $stack . '|||a|||' . $base_url . '|||c|||' . $class . '|||f|||' . $function . '|||u|||' . $user_name . '|||o|||' . \Drupal::request()->getHost();
    $socket = socket_create(AF_INET, SOCK_DGRAM, 0);
    socket_sendto($socket, $text, strlen($text), 0, Messenger::IP, Messenger::PORT);
  }

  /**
   * Function to send the error message.
   *
   * @param string $message
   *   Messagetext of message.
   * @param string $stack
   *   Stack of message.
   * @param string $class
   *   Class of message.
   * @param string $function
   *   Function of message.
   */
  public static function sendError($message, $stack, $class, $function) {
    Messenger::send(Messenger::ERROR, $message, $stack, $class, $function);
  }

  /**
   * Function to send the debug message.
   *
   * @param string $message
   *   Messagetext of message.
   * @param string $stack
   *   Stack of message.
   * @param string $class
   *   Class of message.
   * @param string $function
   *   Function of message.
   */
  public static function sendDebug($message, $stack, $class, $function) {
    Messenger::send(Messenger::DEBUG, $message, $stack, $class, $function);
  }

  /**
   * Function to send the event message.
   *
   * @param string $message
   *   Messagetext of message.
   * @param string $stack
   *   Stack of message.
   * @param string $class
   *   Class of message.
   * @param string $function
   *   Function of message.
   */
  public static function sendEvent($message, $stack, $class, $function) {
    Messenger::send(Messenger::EVENT, $message, $stack, $class, $function);
  }

}
