<?php

namespace Drupal\cdi\Commands;

use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\cdi\Commands
 */
class CdiImportCommands extends DrushCommands {

  /**
   * Drush command that displays the given text.
   *
   * @command cdi:core:import
   * @aliases cdi-ci
   * @usage cdi:core:import
   */
  public function import() {
    cdi_start_import();
  }

}
