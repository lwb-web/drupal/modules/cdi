<?php

namespace Drupal\cdi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class CdiAddContentDeleteForm.
 */
class CdiAddContentDeleteForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cdi.settings.add_content',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cdi_settings_add_content_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $key = NULL) {
    $config = $this->config('cdi.settings.add_content');
    $rules = $config->get('cdi_add_content_rules');
    if (!$key || !$rules[$key]) {
      return $this->redirect('cdi.settings.add_content');
    }

    $form = parent::buildForm($form, $form_state);
    $form['agree'] = [
      '#markup' => $this->t("Delete the entry?"),
      '#weight' => -99,
    ];
    $form['key'] = [
      '#type' => 'hidden',
      '#value' => $key,
    ];

    $form['actions']['submit']['#value'] = $this->t('Delete');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cdi.settings.add_content');
    $values = $form_state->getValues();
    $assignments = $config->get('cdi_add_content_rules');
    if ($values['key'] && $assignments[$values['key']]) {
      unset($assignments[$values['key']]);
      $config
        ->set('cdi_add_content_rules', $assignments)
        ->save();
    }
    parent::submitForm($form, $form_state);
    $form_state->setRedirectUrl(Url::fromRoute('cdi.settings.add_content_rules'));
  }

}
