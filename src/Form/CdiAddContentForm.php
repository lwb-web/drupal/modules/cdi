<?php

namespace Drupal\cdi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\PathElement;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class CdiAddContentForm.
 */
class CdiAddContentForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cdi.settings.add_content',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cdi_settings_add_content_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cdi.settings.add_content');
    $rules = $config->get('cdi_add_content_rules') ?? [];

    // Build table.
    $form['rules'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Path'),
        $this->t('Node Type'),
        $this->t('Operationen'),
      ],
      '#empty' => $this->t('No rules.'),
      '#tableselect' => FALSE,
      '#tree' => TRUE,
    ];

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    $nodeTypes = [];
    foreach ($types as $type) {
      $nodeTypes[str_replace('_', '-', $type->id())] = $type->label();
    }

    $order = 0;
    foreach ($rules as $key => $value) {
      $values = explode("_", $key);

      $form['rules'][$order]['#path'] = $key;

      $form['rules'][$order]['path'] = [
        '#markup' => str_replace(['-', 'wildcard'], ['/', '*'], $values[0])

      ];

      $form['rules'][$order]['types'] = [
        '#markup' => $nodeTypes[$values[1]],
      ];

      $form['rules'][$order]['operation'] = [
        '#type' => 'link',
        '#title' => [
          '#markup' => t('Delete'),
        ],
        '#url' => Url::fromRoute('cdi.settings.add_content_rules.delete', ['key' => $key]),
      ];

      $order++;
    }

    $form['rules'][-1]['#path'] = -1;
    $form['rules'][-1]['path'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Path'),
      '#convert_path' => PathElement::CONVERT_NONE
    ];

    $form['rules'][-1]['type'] = [
      '#type' => 'select',
      '#title' => 'Contenttypen',
      '#title_display' => 'invisible',
      '#options' => $nodeTypes,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cdi.settings.add_content');
    $values = $form_state->getValues()['rules'][-1];

    $path = str_replace(['/', '*'], ['-', 'wildcard'], $values['path']);
    $type = $values['type'];
    $assignments = $config->get('cdi_add_content_rules') ?? [];
    if (!empty($path) && !empty($type)) {

      $key = implode("_", [
        $path,
        $type
      ]);
      $value = [
        $key => 'TRUE',
      ];
      if (!array_key_exists($key, $assignments)) {
        $config
          ->set('cdi_add_content_rules', array_merge($assignments, $value))
          ->save();
      }

    }
    parent::submitForm($form, $form_state);
  }

}
