<?php

namespace Drupal\cdi\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a 'AddContentBlock' block.
 *
 * @Block(
 *  id = "add_content_block",
 *  admin_label = @Translation("Add content block"),
 * )
 */
class AddContentBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('cdi.settings.add_content');
    $rules = $config->get('cdi_add_content_rules');
    $current_path = \Drupal::service('path.current')->getPath();
    $current_alias = \Drupal::service('path_alias.manager')
      ->getAliasByPath($current_path);
    $userCurrent = \Drupal::currentUser();
    $match = FALSE;
    foreach ($rules as $key => $value) {
      $values = explode("_", $key);
      $path = '/' . str_replace(['-', 'wildcard'], ['', '*'], $values[0]);
      if (\Drupal::service('path.matcher')->matchPath($current_alias, $path)) {
        $match = str_replace('-', '_', $values[1]);
        break;
      }
    }
    $markup = '';
    if ($match) {
      $url = Url::fromRoute('node.add', ['node_type' => $match]);
      if ($url->access($userCurrent)) {
        $link = Link::fromTextAndUrl('', $url);
        $link = $link->toRenderable();
        $link['#attributes'] = ['class' => 'icon-add-circle'];
        $markup = \Drupal::service('renderer')->render($link);
      }
    }
    return [
      '#theme' => 'add_content_block',
      '#content' => $markup,
    ];
  }

}
