<?php

namespace Drupal\Tests\cdi\Functional;

use Drupal\cdi\Cdi\CdiTools;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group cdi
 */
class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['cdi', 'node'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Config storage service.
   *
   * @var mixed
   */
  protected $configStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser(
      array_keys(\Drupal::service('user.permissions')->getPermissions())
    );
    $this->drupalLogin($this->user);
    $this->configStorage = \Drupal::service('config.storage');
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests cdi_user defined routes.
   */
  public function testRoutes() {
    $this->drupalGet(Url::fromRoute('cdi.settings'));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(Url::fromRoute('cdi.settings.import'));
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet(Url::fromRoute('cdi.settings.add_content_rules'));
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests cdi_user defined routes.
   */
  public function testDeleteConfig() {
    $this->assertNotEmpty(
      $this->configStorage->read('config_ignore.settings')
    );
    CdiTools::deleteSingleConfigFromModule('config_ignore.settings');
    $this->assertFalse($this->configStorage->read('config_ignore.settings'));
  }

  /**
   * Tests cdi_user defined routes.
   */
  public function testHelpers() {
    $type = $this->drupalCreateContentType();
    $titles = [
      'Single',
      'Multiple',
      'Multiple',
    ];
    $nodes = [];
    foreach ($titles as $title) {
      $nodes[] = $this->drupalCreateNode(
        ['type' => $type->bundle(), 'title' => $title]
      );
    }

    $this->assertEquals(
      CdiTools::entityHasField($nodes[0], 'title'), TRUE,
      'Test entityHasField TRUE'
    );
    $this->assertEquals(
      CdiTools::entityHasField($nodes[0], 'no_field'), FALSE,
      'Test entityHasField FALSE'
    );

    $loadNode = CdiTools::loadEntityById('node', 1);
    $this->assertEquals($nodes[0]->id(), $loadNode->id(), 'Test loadEntityById');

    $loadNode = CdiTools::loadEntityByField('node', 'title', 'Single');
    $this->assertEquals(
      $nodes[0]->id(), $loadNode->id(), 'Test loadEntityByField'
    );

    $loadNodes = CdiTools::loadEntitiesByField('node', ['title' => 'Multiple']);
    $this->assertEquals(count($loadNodes), 2, 'Test loadEntitiesByField');

  }

}
