<?php

namespace Drupal\Tests\cdi\Unit;

use Drupal\cdi\Cdi\CdiTools;
use Drupal\Tests\UnitTestCase;

/**
 * Class CdiTools.
 *
 * @package Drupal\cdi\Cdi
 */
class CdiToolsTest extends UnitTestCase {

  /**
   * Test trims.
   */
  public function testTrimNumeric() {
    $number = "00011";
    $trimNumber = CdiTools::trimNumeric($number);
    $this->assertEquals($trimNumber, 11);
  }

}
